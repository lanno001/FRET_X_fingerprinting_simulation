import argparse, os, sys
import numpy as np

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(f'{__location__}/..')
from helpers import generate_pdb


helix_mod = np.array([[2, -2, 2],
                      [2, 2, 2],
                      [-2, 2, 2],
                      [-2, -2, 2]])




parser = argparse.ArgumentParser(description='Generate artificial lattice structures with defined secondary structures')
parser.add_argument('--seqlen', type=int, required=True)
parser.add_argument('--ss', type=str, required=True,
                    help='One of the following: H(elix)/S(heet)/L(oop).')
parser.add_argument('--out-pdb', type=str, required=True)
args = parser.parse_args()

coords = np.zeros((args.seqlen, 3), dtype=int)

# # determine how many symbols are represented per ss symbol
# nb_ss = args.seqlen // len(args.ss)
# ss_list = [ss * nb_ss for ss in args.ss]

h_idx = 0

for i in range(1, args.seqlen):
    if args.ss == 'H':
        coords[i, :] = coords[i-1] + helix_mod[h_idx]
        h_idx = (h_idx + 1) % 4
    elif args.ss == 'L':
        coords[i, :] = coords[i-1] + np.array([2, 2, 2])

seq = np.array(['A'] * args.seqlen)
pdb_txt = generate_pdb(coords, ['ALA'] * args.seqlen)

with open(args.out_pdb, 'w') as fh:
    fh.write(pdb_txt)

npz_fn = os.path.splitext(args.out_pdb)[0] + '.npz'
np.savez(npz_fn, sequence=seq, coords=coords)
