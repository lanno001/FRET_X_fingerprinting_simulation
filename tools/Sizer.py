import sys


class Sizer(object):
    def __init__(self, obj):
        self.obj = obj

    @property
    def size(self):
        return self.get_size(self.obj)

    def get_size(self, cur_obj, seen=None):
        """Recursively finds size of objects"""
        size = sys.getsizeof(cur_obj)
        if seen is None:
            seen = set()
        obj_id = id(cur_obj)
        if obj_id in seen:
            return 0
        # Important mark as seen *before* entering recursion to gracefully handle
        # self-referential objects
        seen.add(obj_id)
        if isinstance(cur_obj, dict):
            size += sum([self.get_size(v, seen) for v in cur_obj.values()])
            size += sum([self.get_size(k, seen) for k in cur_obj.keys()])
        elif hasattr(cur_obj, '__dict__'):
            size += self.get_size(cur_obj.__dict__, seen)
        elif hasattr(cur_obj, '__iter__') and not isinstance(cur_obj, (str, bytes, bytearray)):
            size += sum([self.get_size(i, seen) for i in cur_obj])
        return size
