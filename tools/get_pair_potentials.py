import argparse
from Bio.PDB import PDBParser, PDBList, PDBIO
from functools import reduce
from itertools import combinations
import os, sys
import pandas as pd
import re
import requests
import multiprocessing as mp
import numpy as np
from random import sample, shuffle
from time import sleep
import warnings

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(f'{__location__}/..')
from helpers import parse_input_dir, parse_output_dir, aa_dict, print_timestamp
from helpers import remove_nonstd_residues, get_pairs_counts




parser = argparse.ArgumentParser(description='Derive pair potentials from a directory of pdb files.')
parser.add_argument('--wd', type=str, required=True,
                    help='working directory')

# --- args for source structures ---
structure_source = parser.add_mutually_exclusive_group(required=True)
structure_source.add_argument('--pdb-dir', type=str,
                    help='Directory containing pdb files to base pair potentials on.')
structure_source.add_argument('--pdb-request', type=str,
                              help='Provide a rest-full request to the pdb database and base pair potentials on'
                                   'the returned structures.')
parser.add_argument('--pdb-subselect', type=int, required=False,
                    help='Use randomly selected subset of provided structures')
parser.add_argument('--fallback', type=str,required=False,
                    help='Space-separated text file contining values to fall back on if certain AA interactions'
                         'are not present in supplied pdbs. If not supplied, fills with NaNs.')

parser.add_argument('--processes', type=int, default=4,
                    help='Number of processes (==structures) to use simultaneously.')
parser.add_argument('--disorder-only', action='store_true',
                    help='base pairs matrix on disordered regions only.')
args = parser.parse_args()

wd = parse_output_dir(args.wd, clean=True)
p = PDBParser(QUIET=True)

pdb_list = []
if args.pdb_dir:
    pdb_list_raw = parse_input_dir(args.pdb_dir, pattern='*.pdb')
    for pdb_fn in pdb_list_raw:
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            pdb_structure = p.get_structure('', pdb_fn)
            if remove_nonstd_residues(pdb_structure): pdb_list.append(pdb_fn)
    if args.pdb_subselect:
        pdb_list = sample(pdb_list, args.pdb_subselect)
elif args.pdb_request:
    pdb_downloader = PDBList()
    pdb_saver = PDBIO()
    wd_pdb = parse_output_dir(f'{wd}pdb')
    wd_pdb_raw = parse_output_dir(f'{wd}pdb_raw')
    response = requests.get(args.pdb_request, timeout=240)
    if response.status_code != 200:
        raise ValueError(f'pdb db request failed with code {response.status_code}')
    req_pattern = re.compile('(?<=pdbChain name=").+(?=")')
    req_nb_success = 0
    req_list = response.text.strip().split('\n')[1:-1]
    if req_nb_success: shuffle(req_list)
    print_threshold = 10
    for eidx, ent in enumerate(req_list):
        try:
            pdb_id, chain_id = re.search(req_pattern, ent).group(0).split('.')
            chain_id = chain_id.upper()
            pdb_fn = pdb_downloader.retrieve_pdb_file(pdb_id, pdir=wd_pdb_raw, file_format='pdb')
            with warnings.catch_warnings():
                warnings.simplefilter('ignore')
                pdb_structure = p.get_structure(pdb_id, pdb_fn)

            pdb_fn_new = f'{wd_pdb}{pdb_id}_{chain_id}.pdb'
            chain = [ch for ch in pdb_structure.get_chains() if ch.get_id() == chain_id][0]
            if remove_nonstd_residues(chain):
                pdb_saver.set_structure(chain)
                pdb_saver.save(pdb_fn_new)
                pdb_list.append(pdb_fn_new)
                req_nb_success += 1
                if req_nb_success > print_threshold:
                    print(f'{print_timestamp()} Structures treated: {eidx}  - downloaded: {req_nb_success}')
                    print_threshold += 10
                if req_nb_success > args.pdb_subselect: break
        except:
            print(f'Error for {ent}, skipping...')
if not len(pdb_list):
    raise ValueError('No suitable structures supplied!')

# --- pre-allocate ---
aa_list = list(aa_dict.keys()) + ['HOH']
aa_list.sort()
# todo implementation changed: index of ss elements not deduced by 1!!!
pair_count_mat = pd.DataFrame(0, columns=aa_list, index=aa_list)
ss_count_mat = pd.DataFrame(0, columns=['H', 'S', 'L'], index=aa_list)

count_dict = {aa: 0 for aa in aa_list}
max_surf_dict = {aa: 0.0 for aa in aa_list}


# --- iterate over pdbs in series ---
if args.processes == 1:
    count_dict = {aa: 0 for aa in aa_list}
    for pdb_fn in pdb_list:
        pair_count_mat, ss_count_mat, count_dict = get_pairs_counts(pdb_fn, (pair_count_mat, ss_count_mat, count_dict),
                                                      False, None, args.disorder_only)

# --- iterate over pdbs in parallel ---
else:
    pair_count_list = [pd.DataFrame(0, columns=aa_list, index=aa_list)] * args.processes
    ss_count_list = [pd.DataFrame(0, columns=['H', 'S', 'L'], index=aa_list)] * args.processes
    count_dict_list = [{aa: 0 for aa in aa_list}] * args.processes
    count_tups = list(zip(pair_count_list, ss_count_list, count_dict_list))
    processes = []
    out_queue = mp.Queue()
    nb_pdbs = len(pdb_list)
    print_threshold = 0
    while len(pdb_list):
        if not out_queue.empty():
            count_tups.append(out_queue.get())
            processes = [proc for proc in processes if proc.is_alive()]
            if nb_pdbs - len(pdb_list) > print_threshold:
                print(f'{print_timestamp()}: {nb_pdbs - len(pdb_list)} structures analysed')
                print_threshold += 100
        if len(processes) < args.processes and len(count_tups):
            processes.append(mp.Process(target=get_pairs_counts, args=(pdb_list.pop(), count_tups.pop(0),
                                                                       True, out_queue, args.disorder_only)))
            processes[-1].start()
        else:
            sleep(1)
    while len(count_tups) < args.processes:
        if not out_queue.empty(): count_tups.append(out_queue.get())
        sleep(1)
    out_queue.close()

    # --- reduce ---
    pair_count_list, ss_count_list, count_dict_list = list(zip(*count_tups))
    pair_count_mat = reduce(lambda x, y: x.add(y, fill_value=0), pair_count_list)
    ss_count_mat = reduce(lambda x, y: x.add(y, fill_value=0), ss_count_list)
    count_dict = reduce(lambda x, y: {res: x[res] + y[res] for res in x}, count_dict_list)

# --- create dataframe of expected values ---
# exp_mat = pd.DataFrame(0, columns=aa_list, index=aa_list)
# aa_comb = combinations(aa_list, 2)
# nb_residues = sum(count_dict.values()) - count_dict['HOH']
# nb_contacts = 4 * (sum(count_dict.values()) - count_dict['HOH']) + count_dict['HOH']
# for a1, a2 in aa_comb:
#     exp_mat.loc[(a1, a2), (a2, a1)] += np.eye(2, dtype=int) * (4 * count_dict[a1] * 4 * count_dict[a2]) / nb_contacts
#     exp_mat.loc[(a1, a2), (a2, a1)] += np.eye(2, dtype=int) * (4 * count_dict[a1] * 4 * count_dict[a2]) / nb_contacts
# for aa in aa_list:
#     # simultaneously sets aa-aa/HOH-HOH to 0, then aa-aa to proper value
#     exp_mat.loc[(aa, 'HOH'), ('HOH', aa)] = np.eye(2, dtype=int) * (count_dict['HOH'] * count_dict[aa] / nb_residues)
#     exp_mat.loc[aa, aa] = (4 * count_dict[aa] * 4 * count_dict[aa]) / nb_contacts
#
# pair_count_mat.replace(0, np.nan, inplace=True)
# e_mat = -1 * np.log(pair_count_mat / exp_mat)
# nb_nans = e_mat.isna().sum().sum()
# if nb_nans > 0:
#     print(f'WARNING: {nb_nans} values in the pairs matrix are NaNs')
#     if args.fallback:
#         print('Patching values with provided fallback matrix')
#         fb_mat = pd.read_csv(args.fallback, sep='\s+', index_col=0, header=None)
#         fb_mat.sort_index(inplace=True)
#         fb_mat.columns = fb_mat.index
#         e_mat.update(fb_mat, overwrite=False)
#
# e_mat.to_csv(f'{wd}/pair_potential_matrix.txt', header=False, index=True, sep=' ', float_format='%.2f')

# similar for ss
tot_helix, tot_sheet, tot_loop = ss_count_mat.sum(axis=0)
tot_resi = tot_helix + tot_sheet + tot_loop
frac_helix, frac_sheet, frac_loop = tot_helix / tot_resi, tot_sheet / tot_resi, tot_loop / tot_resi
res_count = ss_count_mat.sum(axis=1)
ss_expected_df = pd.DataFrame({'H': res_count * frac_helix, 'S': res_count * frac_sheet, 'L': res_count * frac_loop}, index=ss_count_mat.index)
ss_e_df = (ss_count_mat / ss_expected_df).apply(lambda x: np.log(x) * -1)
ss_e_df.to_csv(f'{wd}/ss_potential_matrix.csv', header=True, index=True, float_format='%.3f')

