import os,sys, argparse
from os.path import basename, splitext
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from Bio.PDB import PDBParser
from itertools import combinations

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(f'{__location__}/..')
from helpers import aa_dict_31, get_pairs_mat

parser = argparse.ArgumentParser(description='Draw fully filled energy map for a given sequence')
parser.add_argument('--in-fn', type=str, required=True,
                    help='Fasta or pdb file')
parser.add_argument('--pairsmat', type=str, required=True)
parser.add_argument('--map', type=str, required=True)
args = parser.parse_args()

pdb_parser = PDBParser()

pairs_mat = get_pairs_mat(args.pairsmat)

# Parse sequence
pdb_id, ext = splitext(basename(args.in_fn))
if ext == '.pdb':
    seq = [aa_dict_31.get(res.get_resname(), '') for res in pdb_parser.get_structure('struct', args.in_fn).get_residues()]
    seq = list(''.join(seq))
elif ext == '.fasta':
    seq = None
else:
    raise ValueError('Not a fasta of pdb file')


df_out = pd.DataFrame(0.0, index=seq, columns=seq)
for i1, i2 in combinations(np.arange(len(seq)), 2):
    df_out.iloc[i1, i2] = pairs_mat.loc[seq[i1] , seq[i2]]
    df_out.iloc[i2, i1] = pairs_mat.loc[seq[i1] , seq[i2]]

fig, (ax, ax_cb) = plt.subplots(1, 2, gridspec_kw={'width_ratios': [1,0.04]},
                                figsize=(20,20))
sns.heatmap(df_out, vmin=-1.2, vmax=1.2, cmap='vlag', center=0.0, ax=ax, cbar_ax=ax_cb)
ax.set_aspect('equal','box')
fig.savefig(args.map)
cp=1