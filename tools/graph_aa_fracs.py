import pandas as pd
import os
from Bio import SeqIO
from copy import deepcopy
import seaborn as sns
import matplotlib.pyplot as plt

from helpers import parse_input_dir
import helpers as nhp


fasta_list = parse_input_dir('pdb_entities_unstructured', '*.fasta')
aadf = pd.DataFrame(columns = nhp.aa_dict.keys())
aa_dict_empty = {aa: 0 for aa in nhp.aa_dict.keys()}

for fn in fasta_list:
    cfn = os.path.splitext(os.path.basename(fn))[0]
    seq_gen = SeqIO.parse(fn, 'fasta')
    seq = str(next(seq_gen).seq)
    aa_counts = deepcopy(aa_dict_empty)
    for aa in seq: aa_counts[aa] = aa_counts.get(aa, 0) + 1
    aadf.loc[cfn] = aa_counts

aadf = aadf.apply(lambda x: x * 1.0 / x.sum() if x.sum() != 0 else 0, axis=1)
aadf.reset_index(inplace=True)
mdf = pd.melt(aadf, id_vars='index')
mdf = mdf.rename(index=str, columns={'variable': 'aa', 'value': 'frac', 'index':'protein'})
sns.barplot(x='aa', y='frac', hue='protein', data=mdf)
# plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.figure(dpi=400)
plt.show()
