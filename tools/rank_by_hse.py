from Bio.PDB import PDBParser, HSExposureCA
import pandas as pd
import numpy as np
import argparse
from os.path import basename, splitext
import os
import sys
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(os.path.split(__location__)[0])
from helpers import parse_output_dir, aa_dict_31, parse_input_dir

pdb_parser = PDBParser()

parser = argparse.ArgumentParser(description='rank a list of pdb files on the average HSE of a single given residue type')
parser.add_argument('--in-dir',    type=str, nargs='+',
                    help='pdb files or directory containing pdb files')
parser.add_argument('--res-type', type=str,
                    help='3-letter residue type of interest')
parser.add_argument('--out-csv', type=str,
                    help='output file of ranked pdbs')
args = parser.parse_args()

pdb_list = parse_input_dir(args.in_dir, pattern='*.pdb')
pdbid_list = [basename(splitext(pdb_fn)[0]) for pdb_fn in pdb_list]
out_df = pd.DataFrame(None, index=pdbid_list, columns=['hse'])
nb_pdbs = len(pdbid_list)
counter = 0
print(f'pdbid_list: {len(pdbid_list)}, pdb_list: {len(pdb_list)}')
for pdb_id, pdb_fn in zip(pdbid_list, pdb_list):
    try:
        struct = pdb_parser.get_structure(pdb_id, pdb_fn)
        mod = list(struct)[0]
        hse_list = [res[1] for res in HSExposureCA(radius=12.0, offset=1, model=mod) if res[0].resname == args.res_type]
        if not len(hse_list):
            print(f'Failed for {pdb_id}: no tagged residues')
            counter += 1
            continue
        hse_mean = np.mean([min(hse[:2]) for hse in hse_list])
        out_df.loc[pdb_id, 'hse'] = hse_mean
        counter += 1
        print(f'{counter} / {nb_pdbs} treated ({counter/nb_pdbs * 100}%)')
    except Exception as e:
        print(f'Failed for {pdb_id}: {e}')
        counter += 1
        continue

out_df.sort_values('hse', ascending=True, inplace=True)
out_df.to_csv(args.out_csv)

