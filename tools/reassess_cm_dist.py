import pandas as pd
import itertools
from matplotlib import pyplot as plt
import numpy as np

distdf = pd.read_pickle('dist_df.pkl')
veclens = distdf.loc[:, 'values'].apply(lambda x: len(x))


distdf1000 = distdf.loc[veclens < 1000, :]
dists1000 = list(itertools.chain.from_iterable( list(distdf1000.loc[:,'values'])))
print(np.mean(dists1000))

