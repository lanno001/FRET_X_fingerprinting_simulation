from Bio.PDB import PDBParser, HSExposureCA
import pandas as pd
from shutil import copyfile
import numpy as np
import argparse
from os.path import basename, splitext
import os
import sys
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(os.path.split(__location__)[0])
from helpers import parse_output_dir, aa_dict_31, parse_input_dir

pdb_parser = PDBParser()

parser = argparse.ArgumentParser(description='Given hse ranking csv, select N most exposed and least exposed pdbs and '
                                             'store them in new folders.')
parser.add_argument('--out-dir', type=str,required=True)
parser.add_argument('--in-pdb', type=str, required=True)
parser.add_argument('--in-csv', type=str, required=True)
parser.add_argument('--nb-hits', type=int, default=10)
args = parser.parse_args()

pdb_list = parse_input_dir(args.in_pdb, pattern='*.pdb')
pdb_dict = {basename(splitext(pdb_fn)[0]): pdb_fn for pdb_fn in pdb_list}
out_dir = parse_output_dir(args.out_dir)
out_dir_high = parse_output_dir(out_dir + 'high')
out_dir_low = parse_output_dir(out_dir + 'low')
hse_df = pd.read_csv(args.in_csv, index_col=0)

hse_df = hse_df.query('~hse.isna()').copy()
hse_df.sort_values('hse', inplace=True)
hse_df_low = hse_df.iloc[:args.nb_hits, :]
hse_df_high = hse_df.iloc[-args.nb_hits:, :]

for pdb_id, tup in hse_df_low.iterrows():
    if pdb_id in pdb_dict:
        copyfile(pdb_dict[pdb_id], f'{out_dir_low}{pdb_id}.pdb')
for pdb_id, tup in hse_df_high.iterrows():
    if pdb_id in pdb_dict:
        copyfile(pdb_dict[pdb_id], f'{out_dir_high}{pdb_id}.pdb')
