import argparse, sys, os
import numpy as np
import pandas as pd
from Bio.PDB import PDBParser
from math import sqrt
from itertools import combinations

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(f'{__location__}/..')
from helpers import get_neighbors, inNd, parse_input_dir, parse_output_dir, aa_dict_31, generate_pdb, get_cm, get_diagonal_neighbors, put_cb_on_lattice, get_mock_ss_df

ca_dist = 3.8 / sqrt(3)
cacb_dist = 1.53 / sqrt(3)
n1_dist = 1.48 / sqrt(3)


def get_rotated_poses(coords):
    out_list = []
    for rot in rotmats:
        coords_rot = coords.copy()
        coords_rot[:, :3] = np.matmul(coords[:, :3], rot)
        out_list.append(coords_rot.copy())
    return out_list


def parse_helix_info(helix_txt_list):
    """
    Parse list of strings of PDB HELIX records to a pandas data frame
    """
    helix_df = pd.DataFrame(
        columns=['resi_start', 'resi_end', 'type', 'length']
    )
    for line in helix_txt_list:
        helix_df.loc[int(line[7:10]), :] = [int(line[21:25])+1, int(line[33:37])-1, int(line[39:40]), int(line[72:76])-2]
    return helix_df

def parse_sheet_info(sheet_txt_list):
    """
    Parse list of strings of PDB SHEET records to pandas data frame
    """
    sheet_df = pd.DataFrame(
        columns=['resi_start', 'resi_end', 'orientation', 'resi_hb_cur', 'resi_hb_prev'],
    index=pd.MultiIndex(levels=[[], []], codes=[[], []], names=['strand_id', 'sheet_id']))
    for line in sheet_txt_list:
        ll = line.split()
        if ll[1] == '1':
            sheet_df.loc[(int(ll[1]),ll[2]), :] = [int(line[22:26]), int(line[33:37]), int(line[38:40]),
                                                   pd.NA, pd.NA]
        else:
            sheet_df.loc[(int(ll[1]),ll[2]), :] = [int(line[22:26]), int(line[33:37]), int(line[38:40]),
                                                   int(line[50:54]), int(line[65:69])]
    return sheet_df


rotmats = [np.array([[1,0,0],[0,1,0],[0,0,1]]), np.array([[0,1,0],[1,0,0],[0,0,1]]),
           np.array([[0,0,1],[0,1,0],[1,0,0]]), np.array([[1,0,0],[0,0,1],[0,1,0]])]
mirror_dims = list(combinations([0,1,2], 2)) + [tuple([i]) for i in range(3)] + [(0, 1, 2)]


atm_names_bb = ['N', 'H', 'CA', 'HA', 'C', 'O']
atm_names_res = np.array(['B', 'G', 'D', 'E', 'F'])

parser = argparse.ArgumentParser(description='Parse pdbs into starting structure with folded alpha helices'
                                             'and secondary structure file.')
parser.add_argument('--pdb-dir', type=str, required=True)
parser.add_argument('--cm-type', choices=['ca', 'bb_cm'], default='bb_cm')
parser.add_argument('--out-dir', type=str, required=True)
args = parser.parse_args()

if args.cm_type == 'ca':
    atm_names_bb = ['CA']

pdb_list = parse_input_dir(args.pdb_dir, pattern='*.pdb')
out_dir = parse_output_dir(args.out_dir)

npz_dir = parse_output_dir(out_dir + 'npz')
cm_dir = parse_output_dir(out_dir + 'cm')
pdb_prefold_dir = parse_output_dir(out_dir + 'pdb_prefold')
ss_dir = parse_output_dir(out_dir + 'ss')

for pdb_fn in pdb_list:
    try:
        pdb_id = os.path.splitext(os.path.basename(pdb_fn))[0]
        # out_dir_cur = parse_output_dir(f'{out_dir}{pdb_id}')

        # 1. load structure
        p = PDBParser()
        pdb_structure = p.get_structure('structure', pdb_fn)
        mod = list(pdb_structure.get_models())[0]
        chain = [ch for ch in mod.get_chains()][0]
        seqlen = len(chain)

        # pre-allocate coordinate arrays
        ca_array, ca_array_prefold = np.zeros((seqlen, 3), float), np.zeros((seqlen, 3), int)

        # Gather resn's, coordinates
        resname_list = []
        resi2idx_dict = {}
        cidx = 0
        n_coord = []
        for ri, res in enumerate(chain.get_residues()):
            if res.resname not in aa_dict_31: continue
            resname_list.append(res.resname)
            resi2idx_dict[res.id[1]] = cidx
            atms_bb = []
            # atms_res = []
            for atm in res.get_atoms():
                if atm.get_name() in atm_names_bb:
                    atms_bb.append(atm)
                # elif np.any(np.in1d(list(atm.get_name()), atm_names_res)) \
                #         or res.resname == 'GLY' and atm.get_name() == '2HA':
                #     atms_res.append(atm)  # basically a placeholder
                if ri == 0 and atm.get_name() == 'N':
                    n_coord = atm.get_coord()
            ca_array[cidx] = get_cm(atms_bb)
            cidx += 1

        nb_res = len(resname_list)
        ca_array = ca_array[:nb_res, :]
        ca_array_prefold = ca_array_prefold[:nb_res, :]

        if np.any([resn not in aa_dict_31 for resn in resname_list]):
            print(f'Not all residues are standard in {pdb_id}, skipping...')
            continue

        # Save cm version of structure
        pdb_txt = generate_pdb([np.squeeze(x) for x in np.vsplit(ca_array, len(ca_array))], resname_list,
                               first_n=n_coord)
        with open(f'{cm_dir}{pdb_id}_cm.pdb', 'w') as fh: fh.write(pdb_txt)

        # load secondary structure lines
        ss_df, helix_dict = get_mock_ss_df(pdb_fn, resi2idx_dict, skip_outer_helix_indices=True)


        # Generate coords with prefolded alpha helices
        prefold_indices = []
        for n in range(1, nb_res):
            if n in prefold_indices: continue
            mod = [-1, 1][n % 2]
            ca_array_prefold[n, :] = ca_array_prefold[n - 1, :] + np.array([2 * mod, 2, 2])
            if n in helix_dict:
                res_idx_list, poses = helix_dict[n]
                ca_array_prefold[res_idx_list, :] = poses[0] + ca_array_prefold[n, :]
                prefold_indices.extend(res_idx_list)

        # Save
        np.savez(f'{npz_dir}{pdb_id}_prefold.npz',
                 coords=ca_array_prefold,
                 sequence=np.array([aa_dict_31[aa] for aa in resname_list]),
                 secondary_structure=ss_df.to_numpy())
        pdb_txt = generate_pdb([np.squeeze(x) for x in np.vsplit(ca_array_prefold * 0.5 * ca_dist, len(ca_array_prefold))], resname_list)
        with open(f'{pdb_prefold_dir}{pdb_id}_prefold.pdb', 'w') as fh: fh.write(pdb_txt)

        # Store secondary structure file
        ss_df.to_csv(f'{ss_dir}{pdb_id}_ss.csv')
    except:
        print(f'Conversion failed for {pdb_fn}')
