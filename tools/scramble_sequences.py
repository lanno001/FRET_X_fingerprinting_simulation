import argparse
import os, sys
from random import shuffle

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(f'{__location__}/..')
from helpers import parse_input_dir, parse_output_dir


parser = argparse.ArgumentParser(description='Read single-sequence, no-newline fasta files and return scrambled versions'
                                             '(i.e. same nucleotides/residues but in random order')
parser.add_argument('--in-dir', type=str, required=True)
parser.add_argument('--out-dir', type=str, required=True)
args = parser.parse_args()

fasta_list = parse_input_dir(args.in_dir, pattern='*.fasta')
out_dir = parse_output_dir(args.out_dir, clean=False)

for fn_in in fasta_list:
    with open(fn_in) as fh: header = fh.readline(); seq_list = list(fh.readline())
    shuffle(seq_list)
    seq = ''.join(seq_list)
    fc = header + seq
    fn_out = out_dir + os.path.basename(fn_in)
    with open(fn_out, 'w') as fh: fh.write(fc)
