import argparse, os, sys, re
from os.path import splitext, basename
from math import acos
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
import warnings
import numpy as np
import pandas as pd
from Bio.PDB import PDBParser, PDBIO
from Bio.PDB.Structure import Structure
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(f'{__location__}/..')
import helpers as nhp
from io import StringIO

def get_conect_cards(pdb_fn):
    out_list = []
    with open(pdb_fn, 'r') as fh:
        for line in fh.readlines():
            if line.startswith('CONECT'): out_list.append(line)
    return ''.join(out_list)

def save_pdb(struct, fn, conect):
    io = PDBIO()
    io.set_structure(struct)
    struct_str = StringIO()
    io.save(struct_str)
    out_txt = struct_str.getvalue().rstrip('END\n') + '\n' + conect + '\nEND\n'
    with open(fn, 'w') as fh: fh.write(out_txt)


def dihedral(p):
    """
    source: https://stackoverflow.com/questions/20305272/dihedral-torsion-angle-from-four-points-in-cartesian-coordinates-in-python
    """
    p0 = p[2]
    p1 = p[0]
    p2 = p[1]
    p3 = p[3]

    b0 = -1.0*(p1 - p0)
    b1 = p2 - p1
    b2 = p3 - p2

    # normalize b1 so that it does not influence magnitude of vector
    # rejections that come next
    b1 /= np.linalg.norm(b1)

    # vector rejections
    # v = projection of b0 onto plane perpendicular to b1
    #   = b0 minus component that aligns with b1
    # w = projection of b2 onto plane perpendicular to b1
    #   = b2 minus component that aligns with b1
    v = b0 - np.dot(b0, b1)*b1
    w = b2 - np.dot(b2, b1)*b1

    # angle between v and w in a plane is the torsion angle
    # v and w may not be normalized but that's fine since tan is y/x
    x = np.dot(v, w)
    y = np.dot(np.cross(b1, v), w)
    return np.degrees(np.arctan2(y, x))

pdb_parser = PDBParser()
parser = argparse.ArgumentParser(description='Harvest dye parameters after running model with '
                                             'optimization wrt true FRET value')
parser.add_argument('--pdb-in', type=str, required=True)
parser.add_argument('--angle', type=float, default=0)
parser.add_argument('--dihedral', type=float, default=0)
parser.add_argument('--out-dir', type=str, required=True)

args = parser.parse_args()

pdb_fn_list = nhp.parse_input_dir(args.pdb_in, '*.pdb')
out_dir = nhp.parse_output_dir(args.out_dir)
struct_dir = nhp.parse_output_dir(out_dir + 'structs_by_degrees')
df_list = []

conect_txt = get_conect_cards(pdb_fn_list[0])

for pdb_fn in pdb_fn_list:

    struct_180deg = Structure('d180')
    struct_otherDeg = Structure('dOther')
    mod_count_180deg = 1
    mod_count_other = 1

    pdb_id = basename(splitext(pdb_fn)[0])
    c_cat = re.search('C[0-9]+', pdb_id).group(0)
    struct = pdb_parser.get_structure('pdbid', pdb_fn)
    for mod in struct.get_models():
        incomplete_tag = False

        # dye distances and angles
        tag_res = [res for res in mod.get_residues() if res.id[0] == 'H_TAG']
        tra_list, trb_list = [], []
        for tr in tag_res:
            tra_list.append([atm for atm in tr.get_atoms() if atm.name == 'CA'][0])
            trb_cur = [atm for atm in tr.get_atoms() if atm.name == 'CB']
            if not len(trb_cur):
                incomplete_tag = True
                break
            trb_list.append([atm for atm in tr.get_atoms() if atm.name == 'CB'][0])
        if incomplete_tag:
            continue

        # --- dihedral ---
        coord_list = [tra.coord for tra in tra_list] + [trb.coord for trb in trb_list]  # important: CA1,CA2,CB1,CB2
        dh = nhp.get_abs_dihedral(*coord_list)

        # --- angle ---
        dye_dist = np.linalg.norm(tra_list[0].coord - trb_list[0].coord)
        ca_dist = np.linalg.norm(tra_list[0].coord - tra_list[1].coord)
        trc = [trb.coord - tra.coord for trb, tra in zip(tra_list, trb_list)]
        dye_angle = acos(max(-1, min(1, np.dot(trc[0], trc[1]) / (np.linalg.norm(trc[0]) * np.linalg.norm(trc[1]))))) / np.pi * 180
        dye_dihedral = nhp.get_abs_dihedral(tra_list[0].coord, tra_list[1].coord, trb_list[0].coord, trb_list[1].coord)
        # if dye_angle > args.angle and dye_dihedral > args.dihedral:
        #     mod_copy = mod.copy()
        #     mod_copy.id = mod_count_180deg
        #     struct_180deg.add(mod_copy)
        #     mod_count_180deg += 1
        # else:
        #     mod_copy = mod.copy()
        #     mod_copy.id = mod_count_other
        #     struct_otherDeg.add(mod_copy)
        #     mod_count_other += 1
        efret = nhp.get_FRET_efficiency(np.linalg.norm(trb_list[0] - trb_list[1]))

        df_list.append(pd.Series({'efret': efret, 'ca_dist': ca_dist, 'dye_dist': dye_dist,
                                  'dye_angle': dye_angle, 'dye_dihedral': dh}, name=(pdb_id, c_cat)))

    # --- Save collection structures ---
    # save_pdb(struct_180deg, f'{struct_dir}{pdb_id}_over.pdb', conect_txt)
    # save_pdb(struct_otherDeg, f'{struct_dir}{pdb_id}_under.pdb', conect_txt)

df = pd.concat(df_list, axis=1).T.reset_index().rename({'level_0': 'pdb_id', 'level_1': 'c_cat'}, axis=1)
df.to_csv(f'{out_dir}main_table.csv', index=False)

# --- make summary df ---
summary_list = []
for cc, cdf in df.groupby('c_cat'):
    summary_list.append(pd.Series({
        'c_cat': cc, 'efret': cdf.efret.mean(),
        'dye_dist': cdf.dye_dist.mean(), 'dye_angle': cdf.dye_angle.mean(),
    }))
summary_list.append(pd.Series({
    'c_cat': 'total', 'efret': df.efret.mean(),
    'dye_dist': df.dye_dist.mean(), 'dye_angle': df.dye_angle.mean(),
}))
summary_df = pd.concat(summary_list, axis=1).T
summary_df.to_csv(f'{out_dir}summary_table.csv', index=False)

# --- plotting ---

# Overall
fig = plt.figure(figsize=(15,5))
plt.subplot(131)
sns.violinplot(y='dye_dist', data=df)
plt.subplot(132)
sns.violinplot(y='dye_angle', data=df)
plt.subplot(133)
sns.violinplot(y='efret', data=df)
plt.savefig(f'{out_dir}property_violins.svg')
plt.close(fig)

# per category
fig = plt.figure(figsize=(15,15))
plt.subplot(311)
sns.violinplot(y='dye_dist', x='c_cat', data=df)
plt.subplot(312)
sns.violinplot(y='dye_angle', x='c_cat', data=df)
plt.subplot(313)
sns.violinplot(y='efret', x='c_cat', data=df)
plt.savefig(f'{out_dir}property_violins_per_cat.svg')
plt.close(fig)

# scatters

for ci, (cc, cdf) in enumerate(df.groupby('c_cat')):
    fig = plt.figure(figsize=(15,15))
    sns.scatterplot(x='dye_dihedral', y='dye_angle', hue='ca_dist', s=20, alpha=0.5, data=cdf)
    # sns.displot(x='dye_dihedral', y='dye_angle', kind='kde', data=cdf)
    plt.savefig(f'{out_dir}scatter_dihedral_vs_angle_{cc}.svg')
    plt.close(fig)

fig = plt.figure(figsize=(15,15))
sns.scatterplot(x='dye_dihedral', y='dye_angle', hue='ca_dist', s=20, alpha=0.5, data=df)
# sns.displot(x='dye_dihedral', y='dye_angle', kind='kde', data=cdf)
plt.savefig(f'{out_dir}scatter_dihedral_vs_angle.svg')
plt.close(fig)


for ci, (cc, cdf) in enumerate(df.groupby('c_cat')):
    fig = plt.figure(figsize=(15,15))
    sns.displot(x='ca_dist', y='dye_dihedral', kind='kde', data=cdf)
    plt.savefig(f'{out_dir}kde_ca_dist_vs_dihedral_{cc}.svg')
    plt.close(fig)

fig = plt.figure(figsize=(15,15))
sns.displot(x='ca_dist', y='dye_dihedral', kind='kde', data=df)
plt.savefig(f'{out_dir}kde_ca_dist_vs_dihedral.svg')
plt.close(fig)



for ci, (cc, cdf) in enumerate(df.groupby('c_cat')):
    fig = plt.figure(figsize=(15,15))
    sns.displot(x='dye_dihedral', y='dye_angle', kind='kde', data=cdf)
    plt.savefig(f'{out_dir}kde_dihedral_vs_angle_{cc}.svg')
    plt.close(fig)

fig = plt.figure(figsize=(15,15))
sns.displot(x='dye_dihedral', y='dye_angle', kind='kde', data=df)
plt.savefig(f'{out_dir}kde_dihedral_vs_angle.svg')
plt.close(fig)


for ci, (cc, cdf) in enumerate(df.groupby('c_cat')):
    fig = plt.figure(figsize=(15,15))
    # sns.scatterplot(x='dye_dist', y='dye_angle', s=3, data=cdf)
    sns.displot(x='dye_dist', y='dye_angle', kind='kde', data=cdf)
    plt.savefig(f'{out_dir}scatter_dist_vs_angle_{cc}.svg')
    plt.close(fig)

for ci, (cc, cdf) in enumerate(df.groupby('c_cat')):
    fig = plt.figure(figsize=(15,15))
    # sns.scatterplot(x='ca_dist', y='dye_angle', s=3, data=cdf)
    sns.displot(x='ca_dist', y='dye_angle', kind='kde', data=cdf)
    plt.ylim((0, 200))
    plt.xlim((0, 70))
    plt.savefig(f'{out_dir}scatter_ca_dist_vs_angle_{cc}.svg')
    plt.close(fig)

fig = plt.figure(figsize=(15,15))
# sns.scatterplot(x='ca_dist', y='dye_angle', s=3, data=cdf)
sns.displot(x='ca_dist', y='dye_angle', kind='kde', data=df)
plt.savefig(f'{out_dir}scatter_ca_dist_vs_angle_all.svg')
plt.close(fig)
