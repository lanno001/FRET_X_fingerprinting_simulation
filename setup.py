from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(
    name='lattice_modeling',
    version='0.1',
    packages=['lattice_modeling'],
    python_requires='==3.7',
    install_requires=['numpy', 'plotly', 'cached_property', 'pandas', 'biopython',
                      'pyyaml'],
    author='Carlos de Lannoy',
    author_email='carlos.delannoy@wur.nl',
    description='Generate and analyse lattice models of proteins',
    long_description=readme(),
    license='GPL-3.0',
    keywords='lattice simulation protein folding',
    # url='https://github.com/cvdelannoy/....',
    entry_points={
        'console_scripts': [
            'generate_lattice_models = generate_lattice_models.py'
        ]
    },
    include_package_data=True
)
