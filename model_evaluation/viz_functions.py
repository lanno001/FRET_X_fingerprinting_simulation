import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import linkage, dendrogram
import seaborn as sns
import numpy as np
import matplotlib.gridspec as gridspec
from matplotlib.path import Path


sns.set_style('darkgrid')


def plot_heatmap(df, res, ax, colmap, vlim, cbar=True, clab=None, cbar_shrink=0.6):
    mask_ud = np.zeros_like(df,dtype=bool)
    mask_ud[np.triu_indices_from(df)] = True
    mask_ld = np.invert(mask_ud)
    res_df = df.fillna(res+1) > res
    try:
        linkmap = linkage(res_df.fillna(True).corr())
        no = dendrogram(linkmap, no_plot=True)['leaves']  # new order
    except:
        no = list(range(df.shape[0]))
        pass
    cbar_kws = {"shrink": cbar_shrink, 'label': clab} if clab is not None else None
    sns.heatmap(df.iloc[no, no], annot=False, annot_kws={'size': 1}, mask=mask_ud,
                cmap=colmap, cbar=cbar, cbar_kws=cbar_kws, vmin= vlim[0], vmax=vlim[1],
                xticklabels=False, yticklabels=False, ax=ax)
    sns.heatmap(np.invert(res_df.iloc[no, no]), annot=False, annot_kws={'size': 1}, mask=mask_ld,
                cmap='Greys', cbar=False, xticklabels=False, yticklabels=False, vmin=0, vmax=1, ax=ax)


def plot_uniqueness_scatter(tag_df, unique_colname, tagged_residues, fig, ax):
    # Set residue with largest number of possible tags on x-axis
    res_x = tag_df[tagged_residues].max(axis=0).idxmax()
    res_y = np.array(tagged_residues)[[res != res_x for res in tagged_residues]][0]

    codes = [Path.MOVETO,
             Path.LINETO,
             Path.LINETO,
             Path.LINETO,
             Path.CLOSEPOLY,
             ]

    verts = [
        (-1., 1.),  # left, bottom
        (1., -1.),  # left, top
        (-1., -1.),  # right, top
        (-3., 1.),  # right, bottom
        (-1., 1.),  # ignored
    ]

    sp = ax.scatter(x=tag_df[res_x], y=tag_df[res_y],
                    s=20000, marker=Path(verts, codes),
                    # s=tag_df['total'],
                    c=tag_df[unique_colname],
                    cmap=sns.diverging_palette(10, 240, as_cmap=True, center='light'),
                    vmin=tag_df.frac_discernible.min(), vmax=1.0)
    xticks = np.arange(tag_df[res_x].max()+1)
    xticks_labels = [str(xt) if not xt % 5 else '' for xt in xticks]
    ax.xaxis.set_ticks(xticks)
    ax.set_xticklabels(xticks_labels)
    yticks = np.arange(tag_df[res_y].max() + 1)
    yticks_labels = [str(yt) if not yt % 5 else '' for yt in yticks]
    ax.yaxis.set_ticks(np.arange(tag_df[res_y].max()+1))
    ax.set_yticklabels(yticks_labels)
    ax.set_aspect('equal')

    # colorbar
    cbar = fig.colorbar(sp)
    cbar.set_label('Fraction unique')

    # # marker size legend
    # labs = [int(tag_df.total.min())]
    # sz_options = np.array([5, 10, 25, 50, 100])
    # sz_rough = (tag_df.total.max() - tag_df.total.min()) / 3
    # sz_options = sz_options[sz_options > sz_rough]
    # if len(sz_options):
    #     sz = sz_options[np.argmin(sz_options - sz_rough)]
    #     labs.append(int(labs[-1] // 10 + sz))
    #     while labs[-1] + sz < tag_df.total.max(): labs.append(int(labs[-1] + sz))
    # labs.append(int(tag_df.total.max()))
    # leg_figs = [plt.scatter([], [], s=ps, c='k') for ps in labs]
    # fig.legend(leg_figs, labs, ncol=len(labs), fontsize=10, scatterpoints=1, frameon=True, title='# Structures',
    #            bbox_to_anchor=(0.79, 0.96))

    plt.ylabel(f'# tagged {res_y}')
    plt.xlabel(f'# tagged {res_x}')
    plt.tight_layout()


def plot_uniqueness_heatmap(tag_df, tagged_residues, ax):
    hm_array = np.zeros((tag_df.loc[:, tagged_residues[0]].max() + 1, tag_df.loc[:, tagged_residues[1]].max() + 1))
    hm_array[:, :] = np.nan
    for _, tdr in tag_df.iterrows():
        hm_array[int(tdr.loc[tagged_residues[0]]), int(tdr.loc[tagged_residues[1]])] = tdr.frac_discernible
    # hm_masked = np.ma.masked_array(hm_array, mask=np.isnan(hm_array))
    # ax.set_facecolor('grey')
    # cax = ax.contourf(hm_masked)
    # plt.colorbar(cax)

    hm_ax = sns.heatmap(hm_array, cmap=sns.cubehelix_palette(rot=-.4, reverse=True),
                        ax=ax, cbar_kws={'label': '% Unique structures'})
    hm_ax.invert_yaxis()
    hm_ax.set_xlabel(f'# tagged {tagged_residues[0]}')
    hm_ax.set_ylabel(f'# tagged {tagged_residues[1]}')


def plot_uniqueness_barplot(bp_df, ax, xlab='# tags'):
    col_list = ['#ffffff', '#b2df8a']
    for i, g in enumerate(bp_df.groupby('uniqueness')):
        ax = sns.barplot(data=g[1],
                         x='nb_tags', y='frequency', color=col_list[i % 2],
                         hue=[None, 'method'][i % 2],
                         zorder=i,
                         edgecolor='k', ci=None, ax=ax)
        if xlab is None:
            ax.axes.get_xaxis().get_label().set_visible(False)
    ax.grid(False)
    bp_labs = [str(bl) if not bl % 5 else '' for bl in range(bp_df.nb_tags.max() + 1)]
    ax.set_xticklabels(bp_labs)
    if xlab is not None:
        plt.xlabel(xlab)
    plt.ylabel('# proteins')
    # ax.set_xticklabels(ax.get_xticklabels(), rotation=30)
    ax.legend_.remove()
    ax.tick_params(labelsize=6)


def plot_compound1(bp_df, efret_heatmap_df, dist_heatmap_df,
                   nb_tags_dict_efret, nb_tags_dict_dist, efret_res, dist_res):
    cmap_fret = sns.cubehelix_palette(as_cmap=True, light=.9)
    cmap_dist = sns.cubehelix_palette(as_cmap=True, light=.9, rot=-0.4)
    cmap_fret.set_under('.9')
    cmap_dist.set_under('.9')

    gsfig = plt.figure(constrained_layout=True)
    gs = gridspec.GridSpec(15, 16, figure=gsfig)
    gs_unique = plt.subplot(gs[:5, :])
    gs_h_efret = dict()
    gs_h_dist = dict()
    gs_h_efret[1] = plt.subplot(gs[5:10, :5])
    gs_h_efret[2] = plt.subplot(gs[5:10, 5:10])
    gs_h_efret[3] = plt.subplot(gs[5:10, 10:15])
    gs_h_dist[1] = plt.subplot(gs[10:15, :5])
    gs_h_dist[2] = plt.subplot(gs[10:15, 5:10])
    gs_h_dist[3] = plt.subplot(gs[10:15, 10:15])

    # bar/scatterplot
    plot_uniqueness_barplot(bp_df, gs_unique, xlab=None)

    # heatmaps
    for n in range(1,4):
        if n in nb_tags_dict_efret:
            nl = nb_tags_dict_efret[n]
            plot_heatmap(efret_heatmap_df.loc[nl, nl], efret_res, gs_h_efret[n],
                         cmap_fret, vlim=(0, 0.5), clab='$\Delta E_{FRET,Cy3-Cy5}$',
                         cbar=n == 3, cbar_shrink=1)
        if n in nb_tags_dict_dist:
            nl = nb_tags_dict_dist[n]
            plot_heatmap(dist_heatmap_df.loc[nl, nl], dist_res, gs_h_dist[n],
                         cmap_dist, vlim=(0, 2), clab='$\Delta$ dye pair distance ($\AA$)',
                         cbar=n == 3, cbar_shrink=1)
        gs_h_efret[n].set_aspect('equal')
        gs_h_dist[n].set_aspect('equal')
        if n == 2:
            gs_h_dist[n].set_xlabel(f'{n} \n # tags')
        else:
            gs_h_dist[n].set_xlabel(str(n))
    return gsfig


def plot_uniqueness_line(df, res, nb_tags_dict, ax):
    rel_clustersize_list = []
    for nb_tags in nb_tags_dict:
        nl = nb_tags_dict[nb_tags]
        if nb_tags == 0:  # no tags --> NaN means can't be discerned
            res_df = df.loc[nl, nl].fillna(0) > res
        else:  # tags present --> NaN means not equal in nb of tags per type
            res_df = df.loc[nl,nl].fillna(res+1) > res
        nb_clusters = res_df.drop_duplicates().shape[0]
        rel_clustersize_list.append(nb_clusters / len(nl) * 100)
    sns.pointplot(x=list(nb_tags_dict), y=rel_clustersize_list)
    ax.set_xlabel('total # tags')
    ax.set_ylabel('# clusters / # proteins')



def plot_compound2(bp_df, efret_heatmap_df, dist_heatmap_df,
                   nb_tags_dict_efret, nb_tags_dict_dist, efret_res, dist_res):
    cmap_fret = sns.cubehelix_palette(as_cmap=True, light=.9)
    cmap_dist = sns.cubehelix_palette(as_cmap=True, light=.9, rot=-0.4)
    cmap_fret.set_under('.9')
    cmap_dist.set_under('.9')
    gsfig = plt.figure(constrained_layout=True)
    gs = gridspec.GridSpec(15, 16, figure=gsfig)
    gs_unique = plt.subplot(gs[:5, :])
    gs_line = plt.subplot(gs[5:, :])

    # bar/scatterplot
    plot_uniqueness_barplot(bp_df, gs_unique, xlab=None)

    # nb unique fingerprint lineplot
    plot_uniqueness_line(efret_heatmap_df, efret_res, nb_tags_dict_efret, gs_line)

    #
    # # heatmaps
    # for n in range(1,4):
    #     if n in nb_tags_dict_efret:
    #         nl = nb_tags_dict_efret[n]
    #         plot_heatmap(efret_heatmap_df.loc[nl, nl], efret_res, gs_h_efret[n],
    #                      cmap_fret, vlim=(0, 0.5), clab='$\Delta E_{FRET,Cy3-Cy5}$',
    #                      cbar=n == 3, cbar_shrink=1)
    #     if n in nb_tags_dict_dist:
    #         nl = nb_tags_dict_dist[n]
    #         plot_heatmap(dist_heatmap_df.loc[nl, nl], dist_res, gs_h_dist[n],
    #                      cmap_dist, vlim=(0, 2), clab='$\Delta$ dye pair distance ($\AA$)',
    #                      cbar=n == 3, cbar_shrink=1)
    #     gs_h_efret[n].set_aspect('equal')
    #     gs_h_dist[n].set_aspect('equal')
    #     if n == 2:
    #         gs_h_dist[n].set_xlabel(f'{n} \n # tags')
    #     else:
    #         gs_h_dist[n].set_xlabel(str(n))
    return gsfig


def plot_compound3(tag_df_efret, tagged_residues,
                   efret_heatmap_df, efret_res, nb_tags_dict_efret):
    """
    Plot combining tag-vs-tag heatmap unqiueness and uniqueness per nb tags
    """
    gsfig = plt.figure(constrained_layout=True)
    gs = gridspec.GridSpec(15, 16, figure=gsfig)
    gs_hm = plt.subplot(gs[:5, :])
    gs_line = plt.subplot(gs[5:, :])
    plot_uniqueness_heatmap(tag_df_efret, tagged_residues, ax=gs_hm)
    plot_uniqueness_line(efret_heatmap_df, efret_res, nb_tags_dict_efret, gs_line)
    return gsfig


def plot_efret_heatmaps(efret_heatmap_df, nb_tags_dict, efret_res):
    cmap_fret = sns.cubehelix_palette(as_cmap=True, light=.9)
    cmap_fret.set_under('.9')

    gsfig = plt.figure(constrained_layout=True)
    gs = gridspec.GridSpec(5, 16, figure=gsfig)
    gs_h_efret = dict()
    gs_h_efret[1] = plt.subplot(gs[:, :5])
    gs_h_efret[2] = plt.subplot(gs[:, 5:10])
    gs_h_efret[3] = plt.subplot(gs[:, 10:15])

    # heatmaps
    for n in range(1,4):
        if not n in nb_tags_dict: continue
        nl = nb_tags_dict[n]
        plot_heatmap(efret_heatmap_df.loc[nl, nl], efret_res, gs_h_efret[n],
                     cmap_fret, vlim=(0, 0.5), clab='$\Delta E_{FRET,Cy3-Cy5}$',
                     cbar=n == 3, cbar_shrink=0.33)
        gs_h_efret[n].set_aspect('equal')
        if n == 2:
            gs_h_efret[n].set_xlabel(f'{n} \n # tags')
        else:
            gs_h_efret[n].set_xlabel(str(n))
    return gsfig


def plot_dist_heatmaps(dist_heatmap_df, nb_tags_dict, dist_res):
    cmap_dist = sns.cubehelix_palette(as_cmap=True, light=.9, rot=-0.4)
    cmap_dist.set_under('.9')

    gsfig = plt.figure(constrained_layout=True)
    gs = gridspec.GridSpec(5, 16, figure=gsfig)
    gs_h_dist = dict()
    gs_h_dist[1] = plt.subplot(gs[:, :5])
    gs_h_dist[2] = plt.subplot(gs[:, 5:10])
    gs_h_dist[3] = plt.subplot(gs[:, 10:15])

    # heatmaps
    for n in range(1,4):
        if not n in nb_tags_dict: continue
        nl = nb_tags_dict[n]
        plot_heatmap(dist_heatmap_df.loc[nl, nl], dist_res, gs_h_dist[n],
                     cmap_dist, vlim=(0, 2), clab='$\Delta$ dye pair distance ($\AA$)',
                     cbar=n == 3, cbar_shrink=0.33)
        gs_h_dist[n].set_aspect('equal')
        if n == 2:
            gs_h_dist[n].set_xlabel(f'{n} \n # tags')
        else:
            gs_h_dist[n].set_xlabel(str(n))
    return gsfig


def plot_spiketrains(pattern_dict, plot_fn, sort=False):
    # Plot Efret spiketrains for each structure
    max_nb_spikes = max([len(pm) for pm in pattern_dict.values()])
    stacked_pm = np.zeros((len(pattern_dict), max_nb_spikes))
    names_list = list(pattern_dict)
    names_list.sort(reverse=True)
    for i, pm in enumerate(names_list):
        stacked_pm[i, :len(pattern_dict[pm])] = pattern_dict[pm]
        if len(pattern_dict[pm]):
            stacked_pm[i, len(pattern_dict[pm]):] = pattern_dict[pm][-1]
    if sort:
        no = dendrogram(linkage(stacked_pm, 'complete'), no_plot=True, distance_sort=True)['leaves']
        stacked_pm = stacked_pm[no, :]
        names_list = names_list[no]
    plt.figure(figsize=(20, 10))
    plt.eventplot(stacked_pm, colors='black')
    if len(names_list) <= 20:
        plt.yticks(range(len(names_list)), names_list)
    plt.xlabel('E_fret')
    plt.savefig(plot_fn, dpi=400)
    plt.clf()


def plot_prob_fingerprint(pattern_dict, res_bins, plot_fn, sort=False):
    # Plot Efret spiketrains for each structure
    names_list = list(pattern_dict)
    names_list.sort(reverse=True)
    nb_plots = len(names_list)
    fig = plt.figure(figsize=(2 * nb_plots, 10))
    gs = gridspec.GridSpec(nb_plots, 10, figure=fig)
    for i, pm in enumerate(names_list):
        if i == 0:
            sub = fig.add_subplot(gs[i, :])
            first_sub = sub
        else:
            sub = fig.add_subplot(gs[i, :], sharey=first_sub)
        sns.distplot(pattern_dict[pm], bins=res_bins, ax=sub)
        if i == nb_plots - 1:
            sub.set_xlabel('$E_{FRET}$')
        if i == (nb_plots - 1) // 2:
            sub.set_ylabel('count density')
        else:
            plt.setp(sub.get_xticklabels(), visible=False)
        if i == 0:
            sub.set_xlim(0.0, 1.0)
    plt.savefig(plot_fn, dpi=400)
    plt.clf()
