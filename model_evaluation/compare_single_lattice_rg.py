import argparse, os, sys
import math
import numpy as np
import pandas as pd
from os.path import basename, splitext

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(f'{__location__}/..')

from helpers import parse_output_dir, parse_input_dir, get_pairs_mat
from LatticeModel import LatticeModel

ca_dist = 3.8
cacb_dist = 1.53

def Rg(filename):
    """
    Calculates the Radius of Gyration (Rg) of a protein. Returns the Rg integer value in Angstrom.
    from: https://github.com/sarisabban/Rg/blob/master/Rg.py
    """
    coord = list()
    mass = list()
    with open(filename, 'r') as structure:
        for line in structure:
            try:
                line = line.split()
                x = float(line[6])
                y = float(line[7])
                z = float(line[8])
                coord.append([x, y, z])
                if line[-1] == 'C':
                    mass.append(12.0107)
                elif line[-1] == 'O':
                    mass.append(15.9994)
                elif line[-1] == 'N':
                    mass.append(14.0067)
                elif line[-1] == 'S':
                    mass.append(32.065)
            except:
                pass
    xm = [(m*i, m*j, m*k) for (i, j, k), m in zip(coord, mass)]
    tmass = sum(mass)
    rr = sum(mi*i + mj*j + mk*k for (i, j, k), (mi, mj, mk) in zip(coord, xm))
    mm = sum((sum(i) / tmass)**2 for i in zip(*xm))
    rg = math.sqrt(rr / tmass-mm)
    return round(rg, 3)

pm = get_pairs_mat(f'{__location__}/../potential_matrices/aa_water2_abeln2011.txt')

parser = argparse.ArgumentParser(description='Compare single-lattice models to equally named regular models')
parser.add_argument('--in-lattice', type=str, required=True)
parser.add_argument('--in-pdb', type=str, required=True)
parser.add_argument('--out-tsv', type=str, required=True)
args = parser.parse_args()

pdb_list = parse_input_dir(args.in_pdb, pattern='*.pdb')
npz_list = parse_input_dir(args.in_lattice, pattern='*.npz')

out_df = pd.DataFrame(columns=['rg_true', 'rg_estimate'])

for npz_fn in npz_list:
    bn = splitext(basename(npz_fn))[0]
    pdb_list_cur = [pdb for pdb in pdb_list if bn in pdb]
    if not len(pdb_list_cur) == 1: continue
    pdb_fn = pdb_list_cur[0]
    rg_true = Rg(pdb_fn)
    coord_dict = np.load(npz_fn)
    ca_coords = coord_dict['ca_coords']
    ca_coords[:, :3] = ca_coords[:, :3] / ca_dist

    lm = LatticeModel(pairs_mat=pm, aa_sequence=coord_dict['aa_sequence'], tagged_resi=[],
                      coords=ca_coords, res_coords_mod=coord_dict['res_coords_mod'])
    rg_estimate = lm.rg
    out_df.loc[bn] = {'rg_true': rg_true, 'rg_estimate': rg_estimate}
out_df.to_csv(args.out_tsv, sep='\t', header=True, index=True)

