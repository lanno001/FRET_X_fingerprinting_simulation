import argparse, os, sys, re
from os.path import splitext, basename
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
import math
import warnings
import numpy as np
import pandas as pd
from Bio.PDB.QCPSuperimposer import QCPSuperimposer
from Bio.PDB import PDBParser, PDBList

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(f'{__location__}/..')
import helpers as nhp


def Rg(filename):
    """
    Calculates the Radius of Gyration (Rg) of a protein. Returns the Rg integer value in Angstrom.
    from: https://github.com/sarisabban/Rg/blob/master/Rg.py
    """
    coord = list()
    mass = list()
    with open(filename, 'r') as structure:
        for line in structure:
            try:
                line = line.split()
                if line[0] != 'ATOM': continue
                x = float(line[6])
                y = float(line[7])
                z = float(line[8])
                coord.append([x, y, z])
                if line[-1] == 'C':
                    mass.append(12.0107)
                elif line[-1] == 'O':
                    mass.append(15.9994)
                elif line[-1] == 'N':
                    mass.append(14.0067)
                elif line[-1] == 'S':
                    mass.append(32.065)
            except:
                pass
    xm = [(m*i, m*j, m*k) for (i, j, k), m in zip(coord, mass)]
    tmass = sum(mass)
    rr = sum(mi*i + mj*j + mk*k for (i, j, k), (mi, mj, mk) in zip(coord, xm))
    mm = sum((sum(i) / tmass)**2 for i in zip(*xm))
    rg = math.sqrt(rr / tmass-mm)
    return round(rg, 3)


def rg_from_cm(aa_seq, coord):
    mass = [nhp.aa_mass_dict[aa] for aa in aa_seq]
    xm = [(m*i, m*j, m*k) for (i, j, k), m in zip(coord, mass)]
    tmass = sum(mass)
    rr = sum(mi*i + mj*j + mk*k for (i, j, k), (mi, mj, mk) in zip(coord, xm))
    mm = sum((sum(i) / tmass)**2 for i in zip(*xm))
    rg = math.sqrt(rr / tmass-mm)
    return round(rg, 3)


parser = argparse.ArgumentParser(description='get rg from single lattice model')
parser.add_argument('--in-pdb', type=str, required=True)
args = parser.parse_args()

pdb_parser = PDBParser()
imposer = QCPSuperimposer()

lat_struct = pdb_parser.get_structure('lat', args.in_pdb)
lat_coords = np.vstack([atm.coord for atm in lat_struct.get_atoms() if atm.name == 'CA'])
aa_seq = [nhp.aa_dict_31[res.id[0][2:]] for res in lat_struct.get_residues()]
rg = rg_from_cm(aa_seq, lat_coords)
print(rg)
