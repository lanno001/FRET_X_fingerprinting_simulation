import re
from Bio.PDB import PDBParser
import numpy as np
import matplotlib.pyplot as plt
import argparse
import pandas as pd
import os,sys
import itertools
import math
import seaborn as sns

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(f'{__location__}/..')

import helpers as nhp


def Rg(filename):
    """
    Calculates the Radius of Gyration (Rg) of a protein. Returns the Rg integer value in Angstrom.
    from: https://github.com/sarisabban/Rg/blob/master/Rg.py
    """
    coord = list()
    mass = list()
    with open(filename, 'r') as structure:
        for line in structure:
            try:
                line = line.split()
                x = float(line[6])
                y = float(line[7])
                z = float(line[8])
                coord.append([x, y, z])
                if line[-1] == 'C':
                    mass.append(12.0107)
                elif line[-1] == 'O':
                    mass.append(15.9994)
                elif line[-1] == 'N':
                    mass.append(14.0067)
                elif line[-1] == 'S':
                    mass.append(32.065)
            except:
                pass
    xm = [(m*i, m*j, m*k) for (i, j, k), m in zip(coord, mass)]
    tmass = sum(mass)
    rr = sum(mi*i + mj*j + mk*k for (i, j, k), (mi, mj, mk) in zip(coord, xm))
    mm = sum((sum(i) / tmass)**2 for i in zip(*xm))
    rg = math.sqrt(rr / tmass-mm)
    return round(rg, 3)


parser = argparse.ArgumentParser(description='Takes pdb files of lattice models and compare to reference structures.')

# --- I/O args ---
parser.add_argument('--wd', type=str, required=True,
                    help='working directory')
parser.add_argument('--model-dir', type=str, nargs='+', required=True,
                    help='directory containing pdb files of models.')

# --- argument group for Rg against which comparison will take place ---
comparison_group = parser.add_mutually_exclusive_group(required=True)
comparison_group.add_argument('--rg-list',type=str,
                    help='List of experimental Rg values, from SAXS')
comparison_group.add_argument('--original-dir', type=str, nargs='+',
                    help='directory of ground truth pdb files of models.')
comparison_group.add_argument('--pattern', type=str,
                   help='Treat all names that adhere to the same regex pattern as different models of the same'
                        'peptide.')

# --- plotting args ---
parser.add_argument('--plot-individual-traces', action='store_true',
                    help='Plot Rg over time for individual plots. May take very long!')
args = parser.parse_args()

# path parsing
mod_list = nhp.parse_input_dir(args.model_dir, '*[0-9].pdb')
wd = nhp.parse_output_dir(args.wd, clean=True)
wd_results = nhp.parse_output_dir(wd + 'results/')
wd_sorted = nhp.parse_output_dir(wd + 'pdbs/')
wd_smrg = nhp.parse_output_dir(wd_results + 'single_models/')
wd_psrg = nhp.parse_output_dir(wd_results + 'per_structure/')

# Load ground truth Rg values
if args.original_dir:
    original_list = nhp.parse_input_dir(args.original_dir, '*.pdb')
    original_list.extend(nhp.parse_input_dir(args.original_dir, '*.ent'))
    idp_list = [os.path.splitext(os.path.basename(od))[0] for od in original_list]
    rg_list = [Rg(fn) for fn in original_list]
    rg_exp_df = pd.DataFrame(data={'idp': idp_list, 'rg': rg_list, 'sd': np.zeros(len(rg_list))}).set_index('idp')
    rg_pat = re.compile('(' + '|'.join(idp_list) + ')')
elif args.rg_list:
    rg_exp_df = pd.read_csv(args.rg_list).set_index('idp')
    rg_exp_ids = list(rg_exp_df.index)
    rg_exp_ids.sort(key=len, reverse=True)
    rg_pat = re.compile('('+'|'.join(list(rg_exp_ids)) + ')')
else:
    rg_exp_df = pd.DataFrame(columns=['idp', 'rg', 'sd']).set_index('idp')
    rg_pat = re.compile(args.pattern)

pdb_parser = PDBParser()
multistruct_dict = dict()
rg_df_list = []
for mod_fn in mod_list:
    pdb_fn = os.path.basename(mod_fn)
    pdb_id_ext = os.path.splitext(pdb_fn)[0]
    pdb_id_re = re.search(rg_pat, pdb_id_ext)
    if pdb_id_re is None: continue
    pdb_id = pdb_id_re.group(0)
    multistruct_dict[pdb_id] = multistruct_dict.get(pdb_id, []) + [mod_fn]

    with open(mod_fn, 'r') as fn:
        mod_rl = fn.readlines()
    mod_rg = [mr for mr in mod_rl if 'REMARK   1 RG' in mr]
    mod_rg = [float(re.search('(?<=RG )[0-9.]+', mr).group(0)) for mr in mod_rg]
    # mod_rg = mod_rg[:-1 * len(mod_rg) // 5]
    mod_e = float(re.search('(?<=BASE ENERGY )[\-.0-9]+', mod_rl[0]).group(0))
    seq_len = sum([1 for mr in mod_rl if ' CA ' in mr])
    pro_pct = sum([1 for mr in mod_rl if 'PRO' in mr]) / seq_len * 100
    rg_df_list.append(pd.DataFrame({'pdb_id': pdb_id,
                                    'model': pdb_id_ext,
                                    'energy': mod_e,
                                    'iteration': np.arange(len(mod_rg)),
                                    'rg': mod_rg,
                                    'pro': pro_pct,
                                    'seq_len': seq_len}))


    # individual Rg plot
    if len(mod_rg) and args.plot_individual_traces:
        plt.plot(mod_rg, '-')
        x_max = max(len(mod_rg), 5)
        y_max = max(mod_rg) + 2
        if pdb_id in rg_exp_df.index:
            plt.plot([0, x_max], [rg_exp_df.loc[pdb_id, 'rg']] * 2, '--')
            y_max = max(y_max, rg_exp_df.loc[pdb_id, 'rg'] + 2)
        plt.axis([0, x_max, 0, y_max])
        plt.xlabel('Temp swap')
        plt.ylabel('Rg (A)')
        plt.title(pdb_id)
        plt.savefig(f'{wd_smrg}{pdb_id_ext}_rg_plot.png', dpi=400)
        plt.clf()

rg_df = pd.concat(rg_df_list, axis=0)

# df with rg data/energy of last model in each simulation
rg_last_df = pd.concat([rdf.loc[rdf.iteration == rdf.iteration.max()] for rdf in rg_df_list], axis=0)
rg_avg_df = pd.DataFrame(columns=['mu', 'sd', 'best_E', 'best_E_model', 'rg_best_E_mu', 'rg_best_E_sd', 'experimental', 'seq_len'])

index_tuples = [('sequence', 'length'), ('Proline', '\%')]
index_tuples.extend(list(itertools.product(*(('$R_g$ lattice', '$R_g$ SAXS'), ('$\mu$', '$\sigma$')))))

outdf_colnames = pd.MultiIndex.from_tuples(index_tuples)
outdf = pd.DataFrame(columns=outdf_colnames)

seqlen_dict = {}

# Rg vs time plots
for pid, rg_pid_df in rg_df.groupby('pdb_id'):
    rg_pid_df.reset_index(inplace=True)  # Negate funky thing happening to index which I still don't get
    rg_pid_df.drop('index', axis=1, inplace=True)
# for pid in rg_df['pdb_id'].unique():
#     rg_pid_df = rg_df.loc[rg_df.pdb_id == pid].reset_index()
    rg_last_pid_df = rg_last_df.loc[rg_last_df.pdb_id == pid, :].reset_index()
    if pid in rg_exp_df.index:
        rg_exp = rg_exp_df.loc[pid, 'rg'].round(1)
        rg_exp_sd = rg_exp_df.loc[pid, 'sd'].round(1)
    else:
        rg_exp = np.NaN
        rg_exp_sd = np.NaN

    # Save best and average model stats
    best_E = rg_pid_df.energy.min()
    best_E_idx = rg_pid_df.energy.idxmin()
    best_E_model = rg_pid_df.loc[best_E_idx].model
    best_E_rg_vec = rg_pid_df.loc[rg_pid_df.model == best_E_model].rg
    best_E_rg_mean = best_E_rg_vec.mean()
    best_E_rg_sd = best_E_rg_vec.std()
    seqlen_dict[pid] = rg_last_pid_df.seq_len.mean().astype(int)
    rg_avg_df.loc[pid] = {'mu': rg_last_pid_df.rg.mean(),
                          'sd': rg_last_pid_df.rg.std(),
                          'best_E': best_E,
                          'best_E_model': best_E_model,
                          'rg_best_E_mu': best_E_rg_mean, 'rg_best_E_sd': best_E_rg_sd,
                          'experimental': rg_exp,
                          'seq_len':  rg_last_pid_df.seq_len.mean().astype(int)}
    outdf.loc[pid.replace('_', '\_')] = {('$R_g$ lattice', '$\mu$'): rg_last_pid_df.rg.mean().round(1),
                                         ('$R_g$ lattice', '$\sigma$'): rg_last_pid_df.rg.std().round(1),
                                         ('$R_g$ SAXS', '$\mu$'): rg_exp,
                                         ('$R_g$ SAXS', '$\sigma$'): rg_exp_sd,
                                         ('sequence', 'length'): seqlen_dict[pid],
                                         ('Proline', '\%'): rg_last_pid_df.pro.mean().astype(int)}
    # Rg vs time plot
    ax = sns.lineplot(x='iteration', y='rg', data=rg_pid_df)
    if pid in rg_exp_df.index:
        ax.axhline(rg_exp_df.loc[pid, 'rg'], ls='--')
        # x_max = max(len(mod_rg), 5)
        # plt.plot([0, x_max], [rg_exp_df.loc[pid, 'rg']] * 2, '--')
    plt.xlabel('# acceptances')
    plt.ylabel('Rg ($\AA$)')
    plt.title(pid)
    plt.savefig(f'{wd_psrg}rg_time_{pid}.png', dpi=400)
    plt.clf()

rg_df.sort_values('seq_len', inplace=True)
rg_avg_df.sort_values('seq_len', inplace=True)

plt.figure(figsize=(10,5))
sns.violinplot(x='pdb_id', y='rg', data=rg_df, scale='count', color='#d95f02')
sns.pointplot(x=rg_avg_df.index, y='experimental', data=rg_avg_df, color='#1b9e77')
plt.xlabel(None)
plt.ylabel('Rg')
plt.savefig(f'{wd_results}rg_comparison.png', dpi=400)
plt.savefig(f'{wd_results}rg_comparison.svg')
plt.clf()

dtypes = np.dtype([
    ('pdb_id', str),
    ('rg', float),
    ('seqlen', int)
])
data = np.empty(0, dtype=dtypes)
rg_mean_df = pd.DataFrame(data)
for model, mdf in rg_df.groupby('model'):
    pdb_id = str(mdf.pdb_id.unique()[0])
    rg_mean_df.loc[model, 'rg'] = float(mdf.rg.mean())
    rg_mean_df.loc[model, 'pdb_id'] = pdb_id
    rg_mean_df.loc[model, 'seqlen'] = seqlen_dict[pdb_id]
rg_mean_df.sort_values('seqlen', inplace=True)
plt.figure(figsize=(10,5))
sns.swarmplot(x='pdb_id', y='rg', data=rg_mean_df, color='#d95f02')
sns.pointplot(x=rg_avg_df.index, y='experimental', data=rg_avg_df, color='#1b9e77')
plt.xlabel(None)
plt.ylabel('Rg (A)')
rg_mean_df.to_csv(f'{wd_results}rg_comparison_avg.tsv', sep='\t')
plt.savefig(f'{wd_results}rg_comparison_avg.png', dpi=400)
plt.savefig(f'{wd_results}rg_comparison_avg.svg')
plt.clf()

outdf.sort_values(('sequence', 'length'), inplace=True)
rg_avg_df.to_csv(f'{wd_results}rg_avg_df.tsv', sep='\t')
with open(f'{wd_results}rg_latex_df.txt', 'w') as fh:
    outdf.to_latex(fh, escape=False)

# Rg vs time plot combined
# sns.lineplot(x='iteration', y='rg', hue='pdb_id', data=rg_df)
# plt.xlabel('# acceptances')
# plt.ylabel('Rg ($\AA$)')
# plt.savefig(f'{wd_results}rg_time_all.png', dpi=400)

