import re, os, sys
from Bio.PDB import PDBParser
import matplotlib.pyplot as plt
import argparse
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(f'{__location__}/..')
import helpers as nhp
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import numpy as np
import warnings
import seaborn as sns
import matplotlib.pyplot as plt

from model_evaluation.Structure import Structure
from fingerprint_classification.classification_helpers import get_classification_df


parser = argparse.ArgumentParser(description='Train classifier to recognize Efret fingerprints and classify.')
parser.add_argument('--wd', type=str, required=True,
                    help='working directory')
parser.add_argument('--tagged-residues', type=str, required=True,
                    help='String containing 1-letter codes of tagged residues.')
parser.add_argument('--efret-res', type=float, default=0.01,
                    help='Resolution in FRET efficiency, i.e. minimum difference still considered discernible.')
parser.add_argument('--dist-res', type=float, default=1.0,
                    help='Resolution in distance, i.e. the minimum donor/acceptor difference still considered discernible.')
parser.add_argument('--detection-limit', type=float, default=0.10,
                    help='Minimum fret efficiency still considered readable.')
parser.add_argument('--train', type=str, required=True,
                    help='directory containing training examples.')
parser.add_argument('--test', type=str, required=True,
                    help='directory containing test examples.')
parser.add_argument('--sample-pattern', type=str, default=None,
                   help='Treat all names that adhere to the same regex pattern as different models to be merged into'
                        '1 sample.')
parser.add_argument('--class-pattern', type=str, required=True,
                   help='Treat all names that adhere to the same regex pattern as different models of the same'
                        'peptide (=class).')
parser.add_argument('--observed-tags', action='store_true', help='Compare based on nb seen tags i.o. nb present.')

args = parser.parse_args()
args.tagged_residues = list(args.tagged_residues)

# --- path parsing ---
pdb_list_train = nhp.parse_input_dir(args.train, '*.pdb')
pdb_list_test = nhp.parse_input_dir(args.test, '*.pdb')
if not len(pdb_list_train) or not len(pdb_list_test):
    raise ValueError('No training or test pdbs found')

wd = nhp.parse_output_dir(args.wd, clean=True)
wd_results = nhp.parse_output_dir(wd + 'results/', clean=True)

# --- Fuse multiple structures of same peptide ---
if args.sample_pattern is None:
    sample_pattern = None
else:
    sample_pattern = re.compile(args.sample_pattern)
print(args.class_pattern)
class_pattern = re.compile(args.class_pattern)

X_train, y_train, _ = get_classification_df(pdb_list_train, args.tagged_residues,
                                            sample_pattern=sample_pattern, class_pattern=class_pattern)
X_test, y_test, nb_tags_test = get_classification_df(pdb_list_test, args.tagged_residues,
                                                     sample_pattern=sample_pattern, class_pattern=class_pattern)

# --- train classifier ---
nb_classes = y_train.unique().size
rf = RandomForestClassifier(n_estimators=100, max_depth=nb_classes-1, random_state=0)
# print(X_train)
rf.fit(X_train, y_train)

# --- apply on test data ---
y_pred = rf.predict(X_test)
out_df = pd.DataFrame({'y_pred': y_pred, 'y_test': y_test, 'nb_tags': nb_tags_test})
out_df.loc[:, 'correct'] = out_df.apply(lambda x: x.y_pred == x.y_test, axis=1)

tst_tags_unique = nb_tags_test.unique()

# todo: consider cross validation approach

# --- Plot and save ---
with open(f'{wd_results}df.tsv', 'w') as fh:
    out_df.to_csv(fh, sep='\t')

plot_df = pd.DataFrame({'nb_tags': tst_tags_unique, 'accuracy': [0] * tst_tags_unique.size}).set_index('nb_tags')
for nt, df in out_df.groupby('nb_tags'):
    plot_df.loc[nt, 'accuracy'] = df.correct.mean()
plot_df.reset_index(inplace=True)

with open(f'{wd_results}plot_df.tsv', 'w') as fh:
    plot_df.to_csv(fh, sep='\t')

acc_plot = sns.lineplot('nb_tags', 'accuracy', err_style='bars', data=plot_df)
plt.savefig(f'{wd_results}nbTags_vs_accuracy.png')
