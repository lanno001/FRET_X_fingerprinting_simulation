import re
import pandas as pd
import numpy as np
import warnings
from Bio.PDB import PDBParser

from model_evaluation.Structure import Structure


def pad_fun(x, maxlen):
    pad = np.repeat(0.0, maxlen)
    pad[:len(x)] = x
    return pad

def get_classification_df(pdb_list, tagged_residues, sample_pattern=None, class_pattern=None):
    pdb_parser = PDBParser()
    pdb_id_dict = dict()
    for pdb_f in pdb_list:
        pdb_id = re.search('(?<=/)[^/]+(?=\.(pdb|ent))', pdb_f).group(0)
        if sample_pattern is None:
            pdb_id_dict[pdb_id] = [pdb_f]; continue
        meta_id = re.search(sample_pattern, pdb_id)
        if meta_id is None: continue
        meta_id = meta_id.group(0)
        if meta_id in pdb_id_dict:
            pdb_id_dict[meta_id].append(pdb_f)
        else:
            pdb_id_dict[meta_id] = [pdb_f]

    train_df = pd.DataFrame(index=pdb_id_dict.keys(), columns=['y', 'X', 'nb_tags'], dtype=(str, list))
    for meta_id in pdb_id_dict:
        with open(pdb_id_dict[meta_id][0], 'r') as fh:
            for line in fh.readlines():
                if line.startswith('REMARK   1 FINGERPRINT'):
                    if '[]' in line: continue
                    fp = [float(x) for x in line.lstrip('REMARK   1 FINGERPRINT [').rstrip(']\n ').split(',')]
                    break
        nb_tags = len(fp)
        # with warnings.catch_warnings():
        #     warnings.simplefilter('ignore')
        #     pdb_obj = pdb_parser.get_structure(meta_id, pdb_id_dict[meta_id][0])
        # meta_structure = Structure(pdb_obj, tagged_residues)
        # nb_tags = list(meta_structure.nb_fret_peaks.values())[0]
        # nb_meta_structures = len(pdb_id_dict[meta_id])
        # if nb_meta_structures > 1:
        #     for sidx in range(1, nb_meta_structures):
        #         meta_structure.add_structure(pdb_parser.get_structure(meta_id, pdb_id_dict[meta_id][sidx]))
        # fp = meta_structure.filtered_fret_peaks
        if class_pattern:
            train_df.loc[meta_id, 'y'] = re.search(class_pattern, meta_id).group(0)
        else:
            train_df.loc[meta_id, 'y'] = meta_id
        # train_df.at[meta_id, 'X'] = list(fp.values())[0]
        train_df.at[meta_id, 'X'] = fp
        train_df.at[meta_id, 'nb_tags'] = nb_tags

    X_maxlen = train_df.X.apply(lambda x: len(x)).max()
    X_pad = np.vstack(train_df.X.apply(lambda x: pad_fun(x, X_maxlen)).values)

    return X_pad, train_df.y, train_df.nb_tags
