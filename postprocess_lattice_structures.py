import os
import argparse
from shutil import copyfile
import re

from helpers import parse_input_dir, parse_output_dir

energy_pat = re.compile('(?<=BASE ENERGY )[0-9.-]+')
def get_energy(fn):
    with open(fn, 'r') as fh:
        lin = fh.readline()
    en_gen = re.search(energy_pat, lin)
    if en_gen is not None:
        return float(en_gen.group(0))
    return None

parser = argparse.ArgumentParser(description='Clean up structures folder: '
                                             '(1) remove last structure, in case it did not converge '
                                             '(2) pick top N candidates based on E value.')
parser.add_argument('--wd', type=str, required=True)
parser.add_argument('--lattice-dir', type=str, required=True)
parser.add_argument('--nb-candidates', type=int, default=5)
args = parser.parse_args()

wd = parse_output_dir(args.wd, clean=True)

ld_tuple = list(os.walk(args.lattice_dir))[0]
for l in ld_tuple[1]:
    ld = f'{ld_tuple[0]}/{l}'
    pdb_list = parse_input_dir(ld, pattern='*.pdb')
    pdb_list.sort(key= lambda x: int(re.search('[0-9]+(?=.pdb)', x).group(0)))
    pdb_list = pdb_list[:-1]  # remove last structure
    if len(pdb_list) < args.nb_candidates: continue
    pdb_id = os.path.basename(ld)
    wd_cur = parse_output_dir(f'{wd}{pdb_id}')
    energy_list = [get_energy(fn) for fn in pdb_list]
    # pdb_list = [pdb for it, pdb in enumerate(pdb_list) if energy_list[it] is not None]
    pdb_list.sort(key=get_energy)
    for pf in range(args.nb_candidates):
        copyfile(pdb_list[pf], f'{wd_cur}{os.path.basename(pdb_list[pf])}')
