import requests
import numpy as np
import traceback
import re
from Bio.PDB import PDBList, PDBParser, PDBIO
from Bio.PDB.QCPSuperimposer import QCPSuperimposer
import argparse
from io import StringIO
import warnings
import os
from os.path import basename, splitext
import json
import sys
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(os.path.split(__location__)[0])
from helpers import parse_output_dir, aa_dict_31, parse_input_dir

imposer = QCPSuperimposer()

def is_number(x):
    try:
        float(x)
        return True
    except ValueError:
        return False


def get_query_block(att, op, val):
    return f'''
{{
            "type": "terminal",
            "service": "text",
            "parameters":
              {{"attribute": "{att}",
              "operator": "{op}",
              "value": {val}
              }}
          }}'''

def get_seqmotif_block(rn, nt_min, nt_max):
    motif = f'^([^{rn}]*[{rn}][^{rn}]*){{{{{nt_min},{nt_max}}}}}$'
    return f'''{{
        "query": {{
            "type": "terminal",
            "service": "seqmotif",
            "parameters": {{
                "value": "{motif}",
                "pattern_type": "regex",
                "target": "pdb_protein_sequence"
            }}
        }}'''

def get_query(max_similarity, min_length, max_length, max_rfree, tagged_resn, nt_min, nt_max):
    query_txt = f'''
    http://search.rcsb.org/rcsbsearch/v1/query?json=
    {{
      "query": {{
        "type": "group",
        "logical_operator": "and",
        "nodes": [
{get_query_block('rcsb_cluster_membership.identity', 'equals', max_similarity)},
{get_query_block('rcsb_entry_info.deposited_polymer_entity_instance_count', 'equals', 1)},
{get_query_block('rcsb_entry_info.deposited_polymer_monomer_count', 'greater_or_equal', min_length)},
{get_query_block('rcsb_entry_info.deposited_polymer_monomer_count', 'less_or_equal', max_length)},
{get_query_block('refine.ls_R_factor_R_free', 'less_or_equal', max_rfree)}
        ]
      }},
      "request_options": {{"return_all_hits": true}},
      "return_type": "polymer_instance"
    }}
    '''
    return requests.get(query_txt.replace('\n', ''), allow_redirects=True, timeout=3600)


parser = argparse.ArgumentParser(description='Retrieve representative pdb set with given maximum sequence similarity. '
                                             'Gets highest resolution structure, with a minimum R free cutoff. '
                                             'NOTE: first request to rcsb may fail occassionally,just rerun if '
                                             'this happens.')
parser.add_argument('--max-similarity', type=int, default=30, help='set maximum sequence similarity [default: 30]')
parser.add_argument('--wd', type=str, required=True,
                    help='directory in which results are stored.')
parser.add_argument('--pdb-dir', type=str,
                    help='Instead of Querying rcsb, work with pdb/ent files in this directory.')
parser.add_argument('--max-nb-structures', type=int, default=1000,
                    help='Limit number of structures to find [default: 1000]')
parser.add_argument('--max-rfree', type=float, default=0.21,
                    help='Maximum R-free value for retrieved structure [default: 0.21]')
parser.add_argument('--min-length', type=int, default=3,
                    help='minimum sequence length to retain.')
parser.add_argument('--max-length', type=int,
                    help='maximum sequence length to retain.')
parser.add_argument('--max-nb-tags', type=int,
                    help='optionally, maximum number of tagged amino acids. If provided, also required'
                         'to provide --tagged-resn')
parser.add_argument('--min-nb-tags', type=int,
                    help='optionally, minimum number of tagged amino acids. If provided, also required'
                         'to provide --tagged-resn')
parser.add_argument('--tagged-resn', type=str,
                    help='string of 1-letter codes of tagged amino acids. Required if --max-nb-tags provided')
parser.add_argument('--no-qc', action='store_true',
                    help='Do not filter on Rfree.')
parser.add_argument('--human', action='store_true',
                    help='Only return proteins of human origin')


args = parser.parse_args()

if args.max_nb_tags and not args.tagged_resn:
    raise ValueError('Need to provide --tagged-resn if providing --max-nb-tags')
if args.min_nb_tags and not args.tagged_resn:
    raise ValueError('Need to provide --tagged-resn if providing --min-nb-tags')

wd = parse_output_dir(args.wd, clean=True)
wd_raw = parse_output_dir(wd + 'pdb_raw/')
wd_clean = parse_output_dir(wd + 'pdb_clean/')
failed_up_file = wd + 'failed_uniprots.txt'
errorlog_fn = wd + 'errorlog.txt'

free_r_pattern = re.compile('(?<=FREE R VALUE                     : )[(NULL)0-9.]+')
pdb_parser = PDBParser()
pdb_downloader = PDBList()
url = 'http://www.rcsb.org/pdb/rest/search/?sortfield=Resolution' # note: sorted by resolution
header = {'Content-Type': 'application/x-www-form-urlencoded'}


# Retrieve representatives list
if args.pdb_dir:
    pdb_list = {splitext(basename(pth))[0].replace('pdb', '').upper(): pth for pth in parse_input_dir(args.pdb_dir, pattern='*.ent')}
else:
    rep_request = get_query(args.max_similarity, args.min_length, args.max_length, args.max_rfree,
                            args.tagged_resn, args.min_nb_tags, args.max_nb_tags)
    if rep_request.status_code != 200:
        raise ValueError(f'Request for representatives failed with status {rep_request.status_code}')
    req_dict = json.load(StringIO(rep_request.text))
    pdb_list = [rec['identifier'] for rec in req_dict['result_set']]

# Download each, check for properties and save
struct_dict = {}
structure_counter = 0
for pdb_chain_id in pdb_list:
    try:
        if args.pdb_dir:
            with open(pdb_list[pdb_chain_id], 'r') as fh: pdb_txt = fh.read()
            pdb_id = pdb_chain_id
            pdb_structure = pdb_parser.get_structure(pdb_chain_id, pdb_list[pdb_chain_id])
            chain = list(pdb_structure.get_chains())[0]
            chain_id = chain.id
        else:
            pdb_id, chain_id = pdb_chain_id.split('.')
            pdb_request = requests.get(f'https://files.rcsb.org/download/{pdb_id}.pdb', allow_redirects=True)
            pdb_txt = pdb_request.text
            if '404 Not Found' in pdb_txt:
                with open(failed_up_file, 'a') as el: el.write(f'{pdb_id}\t404 not found\n')
                continue
            if args.human and not 'ORGANISM_SCIENTIFIC: HOMO SAPIENS' in pdb_txt:
                with open(failed_up_file, 'a') as el: el.write(f'{pdb_id}\tRepresentative not from human source\n')
                continue
            if not args.no_qc:
                free_r_search = re.search(free_r_pattern, pdb_txt)
                if free_r_search is None:
                    with open(failed_up_file, 'a') as el: el.write(f'{pdb_id}\tR-free entry not found\n')
                    continue
                free_r = free_r_search.group(0)
                if not is_number(free_r):
                    with open(failed_up_file, 'a') as el: el.write(f'{pdb_id}\tR-free value incorrectly parsed\n')
                    continue
                free_r = float(free_r)
                if free_r > args.max_rfree:
                    with open(failed_up_file, 'a') as el: el.write(f'{pdb_id}\tR-free above cutoff {args.max_rfree}\n')
                    continue

            # Retrieve original pdb
            pdb_fn = pdb_downloader.retrieve_pdb_file(pdb_id, pdir=wd_raw, file_format='pdb')
            # Create Structure object
            with warnings.catch_warnings():
                warnings.simplefilter('ignore')
                pdb_structure = pdb_parser.get_structure(pdb_id, pdb_fn)

            # Retrive chain
            chain_dict = {ch.get_id(): ch for ch in pdb_structure.get_chains()}
            if chain_id not in chain_dict:
                with open(failed_up_file, 'a') as el: el.write(f'{pdb_id}\tchain id {chain_id} not found in pdb file\n')
                continue
            chain = chain_dict[chain_id]

        # remove waters
        [chain.detach_child(res.get_id()) for res in list(chain) if res.get_resname() == 'HOH']

        # Remove non-standard residues that were in first/last position
        res_list = list(chain.get_residues())
        caps = [res_list[0], res_list[-1]]
        while not all([aa_dict_31.get(cap.get_resname(), False) for cap in caps]):
            [chain.detach_child(cap.get_id()) for cap in caps if not aa_dict_31.get(cap.get_resname(), False)]
            res_list = list(chain.get_residues())
            caps = [res_list[0], res_list[-1]]

        # Check for non-standard residues in other positions
        res_list = [res.get_resname() for res in chain.get_residues()]
        non_std_resnames = [res for res in res_list if not aa_dict_31.get(res, False)]
        if len(non_std_resnames):
            with open(failed_up_file, 'a') as fh: fh.write(f'{pdb_chain_id}\tnon-standard res names\n')
            continue
        if len(res_list) < args.min_length:
            with open(failed_up_file, 'a') as fh: fh.write(f'{pdb_chain_id}\tchain too short\n')
            continue
        if args.max_length and len(res_list) > args.max_length:
            with open(failed_up_file, 'a') as fh: fh.write(f'{pdb_chain_id}\tchain too long\n')
            continue
        if args.max_nb_tags or args.min_nb_tags:
            res_list_1 = [aa_dict_31[res] for res in res_list]
            nb_tags = 0
            for r in args.tagged_resn:
                nb_tags += sum([res == r for res in res_list_1])
            if args.max_nb_tags:
                if nb_tags > args.max_nb_tags:
                    with open(failed_up_file, 'a') as fh: fh.write(f'{pdb_chain_id}\ttoo many tagged residues\n')
                    continue
            if args.min_nb_tags:
                if nb_tags < args.min_nb_tags:
                    with open(failed_up_file, 'a') as fh: fh.write(f'{pdb_chain_id}\ttoo few tagged residues\n')
                    continue

        # Check rmsd against previously stored chains: avoids structural copies which for one FUCKED UP reason or the other are present even if I explicitly ask for representative entities
        ca_coords = np.vstack([atm.coord for atm in chain.get_atoms() if atm.name == 'CA'])
        nb_ca = len(ca_coords)
        if nb_ca in struct_dict:
            rmsd_list = []
            clone_found = False
            for target in struct_dict[nb_ca]:
                imposer.set(target, ca_coords)
                imposer.run()
                rmsd = imposer.get_rms()
                if rmsd < 2.0:
                    with open(failed_up_file, 'a') as fh: fh.write(f'{pdb_chain_id}\tstructurally identical to other structure')
                    clone_found = True
                    break
            if clone_found: continue

        # Save correct chain
        io = PDBIO()
        io.set_structure(chain)
        io.save(f'{wd_clean}{pdb_id}.pdb')

        # Add secondary structure info
        pdb_txt_list = pdb_txt.split('\n')
        ss_list = [l for l in pdb_txt_list if l.startswith('HELIX') and l[19] == chain_id] + \
                  [l for l in pdb_txt_list if l.startswith('SHEET') and l[21] == chain_id]
        ss_txt = '\n'.join(ss_list)
        with open(f'{wd_clean}{pdb_id}.pdb', 'r') as fh: coord_txt = fh.read()
        with open(f'{wd_clean}{pdb_id}.pdb', 'w') as fh: fh.write(ss_txt + '\n' + coord_txt)
        structure_counter += 1

        if nb_ca in struct_dict:
            struct_dict[nb_ca].append(ca_coords)
        else:
            struct_dict[nb_ca] = [ca_coords]

        if structure_counter >= args.max_nb_structures:
            break
    except Exception as e:
        with open(failed_up_file, 'a') as el:
            el.write(f'{pdb_chain_id.split(".")[0]}\tException in script: {e}, tb: {traceback.format_exc()}\n')
        if os.path.exists(f'{wd_clean}{pdb_id}.pdb'):
            os.remove(f'{wd_clean}{pdb_id}.pdb')
        continue
