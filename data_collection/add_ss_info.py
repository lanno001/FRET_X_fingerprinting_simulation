import argparse, os, sys, re

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(f'{__location__}/..')

from helpers import parse_input_dir, parse_output_dir

parser = argparse.ArgumentParser(description='Add secondary structure information back in pdb files.')
parser.add_argument('--pdb-dir', type=str, required=True,
                    help='ent file directory to which you wich ss information to add')
parser.add_argument('--original-dir', type=str, required=True,
                    help='pdb/ent file directory from which ss information should be taken')
parser.add_argument('--out-dir', type=str, required=True)

args = parser.parse_args()

pdb_list = parse_input_dir(args.pdb_dir, pattern='*.pdb')
original_list = parse_input_dir(args.original_dir, pattern='*.ent')
original_dict = {re.search('(?<=pdb)[0-9a-z]+(?=.ent)', ori).group(0): ori for ori in original_list}
out_dir = parse_output_dir(args.out_dir, clean=False)

for pdb_fn in pdb_list:
    pdb_id = os.path.splitext(os.path.basename(pdb_fn))[0]
    original_fn = original_dict.get(pdb_id.lower(), None)
    if original_fn is None: continue

    # Retrieve chain id
    with open(pdb_fn, 'r') as fh: pdb_txt = fh.read()
    try:
        chain_id = re.search('(?<=ATOM.{17})[A-Z]', pdb_txt).group(0)
    except:
        ValueError(f'no valid chain id in {pdb_id}')
    # Remove present secondary structure information, if any
    pdb_txt_list = pdb_txt.split('\n')
    pdb_txt_list = [pl for pl in pdb_txt_list if not (pl.startswith('HELIX') or pl.startswith('SHEET'))]
    pdb_txt = '\n'.join(pdb_txt_list)

    # Add secondary structure info
    with open(original_fn, 'r') as fh: original_txt = fh.readlines()
    ss_list = [l for l in original_txt if l.startswith('HELIX') and l[19] == chain_id] + \
              [l for l in original_txt if l.startswith('SHEET') and l[21] == chain_id]
    ss_txt = ''.join(ss_list)
    with open(f'{out_dir}{pdb_id}.pdb', 'w') as fh: fh.write(ss_txt + '\n' + pdb_txt)
