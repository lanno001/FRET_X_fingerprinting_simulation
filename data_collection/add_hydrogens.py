import os, sys, argparse
import pyrosetta
import shutil, pathlib
from glob import glob

pyrosetta.init()

# --- IO ---
def parse_output_dir(out_dir, clean=False):
    out_dir = os.path.abspath(out_dir) + '/'
    if clean:
        shutil.rmtree(out_dir, ignore_errors=True)
    pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)
    return out_dir


def parse_input_dir(in_dir, pattern=None):
    if type(in_dir) != list: in_dir = [in_dir]
    out_list = []
    for ind in in_dir:
        if not os.path.exists(ind):
            raise ValueError(f'{ind} does not exist')
        if os.path.isdir(ind):
            ind = os.path.abspath(ind)
            if pattern is None: out_list.extend(glob(f'{ind}/**/*', recursive=True))
            else: out_list.extend(glob(f'{ind}/**/{pattern}', recursive=True))
        else:
            if pattern is None: out_list.append(ind)
            elif pattern.strip('*') in ind: out_list.append(ind)
    return out_list
# ---

parser = argparse.ArgumentParser(description='Add missing heavy non-backbone and H atoms '
                                             'to pdb files using rosetta')
parser.add_argument('--in-dir', type=str, required=True)
parser.add_argument('--out-dir', type=str, required=True)
parser.add_argument('--restart',action='store_true',
                    help='Pick up where a crashed run stopped')

args = parser.parse_args()

pdb_list = parse_input_dir(args.in_dir, pattern='*.pdb')
out_dir = parse_output_dir(args.out_dir, clean=False)

for pdb_fn in pdb_list:
    pdb_id = os.path.splitext(os.path.basename(pdb_fn))[0]
    out_fn = f'{out_dir}{pdb_id}.pdb'
    if args.restart and os.path.exists(out_fn):
        print(f'skipping {pdb_id}, exists')
        continue
    try:
        pose = pyrosetta.pose_from_pdb(pdb_fn)
    except Exception as e:
        print(f'pyrosetta could not load {pdb_id}, original error message: {e}')
        continue
    pose.dump_pdb(out_fn)
