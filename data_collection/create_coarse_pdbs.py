from Bio.PDB import PDBParser
import os, sys, argparse
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.stats import norm
from sklearn.mixture import GaussianMixture
import seaborn as sns
import warnings
import itertools
import pandas as pd

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(f'{__location__}/..')

import helpers as nhp

def get_angle(coords):
    """
    Return angle between three sets of coordinates
    :param coords: 3x3 numpy array, vectors in rows, dims in columns
    :return: float, angle in degrees
    """
    cc = coords - coords[1, :]
    cc[0, :] = cc[0, :] / np.linalg.norm(cc[0, :])
    cc[2, :] = cc[2, :] / np.linalg.norm(cc[2, :])
    return np.arccos(np.clip(np.dot(cc[0, :], cc[2, :]), -1.0, 1.0)) * (180 / np.pi)

def plot_quiver(u,v,w):
    x = np.zeros_like(u)
    y = np.zeros_like(v)
    z = np.zeros_like(w)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.quiver(x,y,z,u,v,w)
    ax.set_xlim([-1,1])
    ax.set_ylim([-1, 1])
    ax.set_zlim([-1, 1])
    plt.show()

def mpl_add_pdf(x_original, mu, sd, ax, label):
    x = np.linspace(np.min(x_original), np.max(x_original), 1000)
    y = norm.pdf(x, loc=mu, scale=sd)
    ax.plot(x,y, lw=2, label=label)



atm_names_bb = ['N', 'H', 'CA', 'HA', 'C', 'O']
atm_names_res = np.array(['B', 'G', 'D', 'E', 'F'])

pdb_parser = PDBParser()

parser = argparse.ArgumentParser(description='Generate coarse grained pdbs of regular full-atom pdbs, with one object,'
                                             'denoted as CA, at the center-of-mass of each original aa. Also records '
                                             'distances between mass centers.')
parser.add_argument('--in-dir', type=str,
                    help='entity file or directory containing entity files of which to generate coarse-grained versions')
parser.add_argument('--cm-type', type=str, choices=['bb', 'whole', 'ca'], default='bb',
                    help='Declare how to define the center of mass: using backbone only (bb) or whole residue (whole). [default: bb]')
parser.add_argument('--out-dir', type=str, required=True,
                    help='Location where coarse pdb files are stored.')
args = parser.parse_args()


ent_list = nhp.parse_input_dir(args.in_dir, '*.pdb')
out_dir = nhp.parse_output_dir(args.out_dir)
out_dir_structs = nhp.parse_output_dir(f'{out_dir}coarse_structures')
nb_aa = len(nhp.aa_dict_31)
dist_df = pd.DataFrame({k: [[]] * nb_aa for k in nhp.aa_dict_31.keys()},
                       columns = nhp.aa_dict_31.keys(), index=nhp.aa_dict_31.keys())
angles_dict = {rn:[] for rn in nhp.aa_dict_31.keys()}
cm_dist_list = []
pdb_id_list = []

for ent in ent_list:
    pdb_id, ext = os.path.splitext(os.path.basename(ent))
    print(f'Loading entity {pdb_id}')
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        pdb_structure = pdb_parser.get_structure(pdb_id, ent)
    chain_list = list(pdb_structure.get_chains())
    if len(chain_list) > 1:
        print(f'Warning! {pdb_id}{ext} contains {len(chain_list)} chains, ensure files contain 1 chain to avoid ambiguity!')
        # note: if multiple chains, pick first (alphabetic order). Not perfect, but works in often occuring logical
        # note: naming schemes, e.g. H(eavy)/L(ight), E(nzyme)/S(ubstrate), regular alphabetic order.
        chainid_list = [ch.get_id() for ch in chain_list]
        chainid_list.sort()
        chainid = chainid_list[0]
        chain = [ch for ch in chain_list if ch.get_id() == chainid][0]
    elif len(chain_list) == 1:
        chain = chain_list[0]
    else:
        print(f'No (named) chains found in {ent}, skipping...')
        continue
    cm_bb_list = [] # backbone centers of mass
    cm_res_list = [] # side chain centers of mass

    # coords = []
    res_names = []
    # Get center of mass of each backbone and residue segment
    for res in chain.get_residues():
        res_name = res.get_resname()
        if res_name not in list(nhp.aa_dict_31): continue
        res_names.append(res_name)
        if args.cm_type == 'bb':
            bb_atms = [atm for atm in res.get_atoms() if atm.name in atm_names_bb]
        elif args.cm_type == 'ca':
            bb_atms = [atm for atm in res.get_atoms() if atm.name == 'CA']
        else:
            bb_atms = [atm for atm in res.get_atoms()]
        res_atms = [atm for atm in res.get_atoms() if np.any(np.in1d(list(atm.name), atm_names_res))]
        if res.get_resname() == 'GLY': res_atms.append([atm for atm in res.get_atoms() if atm.name == '2HA'][0])
        if not len(bb_atms) or not len(res_atms): continue
        cm_bb_list.append(nhp.get_cm(bb_atms))
        cm_res_list.append(nhp.get_cm(res_atms))
        # res_coords = [atm.get_coord() * atm.mass for atm in res.get_atoms()]
        # res_coords = np.sum(np.row_stack(res_coords), axis=0)
        # tm = np.sum([atm.mass for atm in res.get_atoms()])
        # coords.append(res_coords / tm)
    if len(cm_bb_list) == 0: continue
    cm_bb_array = np.vstack(cm_bb_list)

    # Get distances between adjacent residue mass centers
    cs = cm_bb_list[0]
    cm_dists = []
    for cidx, coord in enumerate(cm_bb_list[1:]):
        d = np.sqrt(np.sum(np.power(cs - coord, 2)))
        dist_df.loc[res_names[cidx], res_names[cidx + 1]] = dist_df.loc[res_names[cidx], res_names[cidx + 1]] + [d]
        cm_dists.append(d)
        cs = coord

    # Get angles per set of three CAs
    for cidx in range(2, len(cm_bb_list) - 1):
        angles_dict[res_names[cidx]].append(get_angle(cm_bb_array[cidx-2:cidx+1]))

    cm_dist_list.append(cm_dists)
    cm_mean_dist = np.mean(cm_dists)
    cm_sd_dist = np.std(cm_dists)

    # write to pdb file
    coord_txt_list = []
    conect_txt_list = []

    pdb_txt = []
    pdb_txt.append(f'REMARK   0 CMD MEAN {cm_mean_dist}\n')
    pdb_txt.append(f'REMARK   0 CMD SD {cm_sd_dist}\n')
    pdb_txt.append(nhp.generate_pdb(cm_bb_list, res_names, cm_res_list))
    pdb_txt = ''.join(pdb_txt)
    out_file = f'{out_dir_structs}{pdb_id}.pdb'
    with open(out_file, 'w') as fh:
        fh.write(pdb_txt)

    pdb_id_list.append(pdb_id)
cm_dist_all = np.array(list(itertools.chain.from_iterable(cm_dist_list)))
cm_angle_all = np.array(list(itertools.chain.from_iterable(angles_dict.values())))
cm_angle_df = pd.concat([pd.DataFrame({'angle': angles_dict[res], 'resname': res}) for res in angles_dict])

# Plotting distance statistics
dist_gm = GaussianMixture(2).fit(cm_dist_all.reshape(-1,1)) # Expecting a bimodal dist
means, sds = dist_gm.means_.squeeze(), dist_gm.covariances_.squeeze()

plt.figure()
plt.xlabel('distance (${\AA}$)')
plt.ylabel('frequency')
plt.title('mass center distance between adjacent AAs')
ax = sns.distplot(cm_dist_all, kde=False, norm_hist=True)
mpl_add_pdf(cm_dist_all, means[0], sds[0], ax, f'dist1: {np.round(means[0], 3)}')
mpl_add_pdf(cm_dist_all, means[1], sds[1], ax, f'dist2: {np.round(means[1], 3)}')
plt.legend()
plt.savefig(f'{out_dir}mcd_distribution.svg')
plt.close()

plt.figure()
plt.xlabel('distance (${\AA}$)')
plt.ylabel('frequency')
plt.title('mass center distance between adjacent AAs')
cm_dist_lt8 = cm_dist_all[cm_dist_all < 8]
ax = sns.distplot(cm_dist_lt8, kde=False, norm_hist=True)
mpl_add_pdf(cm_dist_all, means[0], sds[0], ax, f'dist1: {np.round(means[0], 3)}')
mpl_add_pdf(cm_dist_all, means[1], sds[1], ax, f'dist2: {np.round(means[1], 3)}')
plt.legend()
plt.savefig(f'{out_dir}mcd_distribution_below8.svg')
plt.close()

# Plotting angle statistics
angle_gm = GaussianMixture(2).fit(cm_angle_all.reshape(-1,1)) # Expecting a bimodal dist
means, sds = angle_gm.means_.squeeze(), angle_gm.covariances_.squeeze()

plt.figure()
ax = plt.gca()
plt.xlabel('angle (deg)')
plt.ylabel('frequency')
plt.title('angles between backbone mass centers')
ax = sns.distplot(cm_angle_all, kde=False, norm_hist=True, ax=ax)
mpl_add_pdf(cm_angle_all, means[0], sds[0], ax, f'dist1: {np.round(means[0], 3)}')
mpl_add_pdf(cm_angle_all, means[1], sds[1], ax, f'dist2: {np.round(means[1], 3)}')
plt.legend()
plt.savefig(f'{out_dir}mcd_distribution.svg')
plt.close()

plt.figure(figsize=[10,5])
sns.violinplot(x='resname', y='angle', data=cm_angle_df)
plt.savefig(f'{out_dir}mcangle_distribution.svg')
plt.close()

cm_avg_angle_df = pd.DataFrame(index=list(angles_dict), columns=['mean_angle', 'sd_angle', 'median_angle',
                                                                'bin_mean1', 'bin_sd1', 'bin_mean2', 'bin_sd2'])
for res in angles_dict:
    dist_gm = GaussianMixture(2, n_init=10).fit(np.array(angles_dict[res]).reshape(-1, 1))  # Expecting a bimodal dist
    means, sds = dist_gm.means_.squeeze(), dist_gm.covariances_.squeeze()
    cm_avg_angle_df.loc[res] = (np.mean(angles_dict[res]), np.std(angles_dict[res]), np.median(angles_dict[res]),
                                means[0], sds[0], means[1], sds[1])

for cm in cm_dist_list:
    sns.distplot(cm, hist=False)
plt.savefig(f'{out_dir}mcd_distribution_per_pdb.png')
plt.close()

for cm in cm_dist_list:
    sns.distplot([c for c in cm if c < 8], hist=False)
plt.savefig(f'{out_dir}mcd_distribution_per_pdb_below8.png')
plt.close()

for res in angles_dict:
    if not len(angles_dict[res]): continue


# Print stats and save
print(f'Median mass center distance: {np.median(cm_dist_all)}')
print(f'Mean mass center distance: {np.mean(cm_dist_all)}, sd: {np.std(cm_dist_all)}')
print(f'Mean angle: {np.mean(cm_angle_all)}')
np.savez(f'{out_dir}dists_and_angles.npz', dists=cm_dist_all, angles=cm_angle_all)
cm_mean = np.mean(cm_dist_all)
dist_df = dist_df.applymap(lambda x: np.mean(x) if len(x) != 0 else cm_mean)
dist_df.to_pickle(f'{out_dir}aa_pairwise_dist_df.pkl')
dist_df.to_csv(f'{out_dir}pairwise_dist_df.csv')
dist_df = pd.DataFrame({'id': pdb_id_list, 'values': cm_dist_list})
dist_df.to_pickle(f'{out_dir}dist_df.pkl')
dist_df.to_csv(f'{out_dir}dist_df.csv')
cm_avg_angle_df.to_csv(f'{out_dir}angle_avg_df.csv')
