import sys, os
from os.path import basename, splitext
from Bio.PDB import PDBParser
import pandas as pd

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
sys.path.append(f'{__location__}/..')

from helpers import parse_input_dir

pdb_parser = PDBParser()
pdb_list = parse_input_dir(sys.argv[1], pattern='*.pdb')
out_df = pd.DataFrame(index=pdb_list, columns=['pdb_id', 'sequence_length'])
for pdb_fn in pdb_list:
    pdb_id = splitext(basename(pdb_fn))[0]
    pdb_structure = pdb_parser.get_structure(pdb_id, pdb_fn)
    seqlen = len(list(pdb_structure.get_residues()))
    out_df.loc[pdb_fn] = [pdb_id, seqlen]

out_df.reset_index(inplace=True)
out_df.drop('index', axis=1, inplace=True)
out_df.sort_values('sequence_length', inplace=True)
out_df.to_csv(sys.stdout, sep='\t', index=False)
