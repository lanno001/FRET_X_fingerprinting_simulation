import requests
import re
from Bio.PDB import PDBList, PDBParser, PDBIO
import argparse
import warnings

from helpers import parse_output_dir, aa_dict_31

def is_number(x):
    try:
        float(x)
        return True
    except ValueError:
        return False

parser = argparse.ArgumentParser(description='Retrieve pdbs matching uniprot IDs. Gets highest resolution structure,'
                                             'with a minimum R free cutoff.')
parser.add_argument('--uniprot-list', type=str, required=True,
                    help='txt file containing target uniprot IDs, one per line.')
parser.add_argument('--wd', type=str, required=True,
                    help='directory in which results are stored.')
parser.add_argument('--min-rfree', type=float, default=0.28,
                    help='Maximum R-free value for retrieved structure.')
parser.add_argument('--min-length', type=int, default=3,
                    help='minimum sequence length to retain.')
parser.add_argument('--no-qc', action='store_true',
                    help='Do not filter on Rfree.')
args = parser.parse_args()

wd = parse_output_dir(args.wd, clean=True)
wd_raw = parse_output_dir(wd + 'pdb_raw/')
wd_clean = parse_output_dir(wd + 'pdb_clean/')
failed_up_file = wd + 'failed_uniprots.txt'
errorlog_fn = wd + 'errorlog.txt'
free_r_cutoff = args.min_rfree

free_r_pattern = re.compile('(?<=FREE R VALUE                     : )[(NULL)0-9.]+')
pdb_parser = PDBParser()
pdb_downloader = PDBList()
url = 'http://www.rcsb.org/pdb/rest/search/?sortfield=Resolution' # note: sorted by resolution
header = {'Content-Type': 'application/x-www-form-urlencoded'}
with open(args.uniprot_list, 'r') as fh:
    uniprot_list = fh.read()
uniprot_list = uniprot_list.split('\n')

for up in uniprot_list:
    try:
        query_text = f"""
        <orgPdbQuery>
        <queryType>org.pdb.query.simple.UpAccessionIdQuery</queryType>
        <description>Simple query for uniprot accession IDs</description>
        <accessionIdList>{up}</accessionIdList>
        </orgPdbQuery>
        """
        response = requests.post(url, data=query_text, headers=header)

        if response.status_code != 200:
            print(f"Failed to retrieve results from PDB for {up}")
            with open(failed_up_file, 'a') as fh: fh.write(f'{up}\n')
            continue
        pdb_list = response.text.strip().split('\n')

        # Check free R score of pdb files in order of increasing resolution
        pdb_hit_bool = False
        pdb_idx = 0
        for pdb_id_version in pdb_list:
            if ':' in pdb_id_version:
                pdb_id, entity_id = pdb_id_version.split(':')
            else:
                pdb_id = pdb_id_version
                entity_id = ''
            pdb_request = requests.get(f'https://files.rcsb.org/download/{pdb_id}.pdb', allow_redirects=True)
            pdb_txt = pdb_request.text
            if '404 Not Found' in pdb_txt:
                with open(failed_up_file, 'a') as el: el.write(f'{up}\t{pdb_id}\t404 not found\n')
                continue
            if not args.no_qc:
                free_r_search = re.search(free_r_pattern, pdb_txt)
                if free_r_search is None:
                    with open(failed_up_file, 'a') as el: el.write(f'{up}\t{pdb_id}\tR-free entry not found\n')
                    continue
                free_r = free_r_search.group(0)
                if not is_number(free_r):
                    with open(failed_up_file, 'a') as el: el.write(f'{up}\t{pdb_id}\tR-free value incorrectly parsed\n')
                    continue
                free_r = float(free_r)
                if free_r > free_r_cutoff:
                    with open(failed_up_file, 'a') as el: el.write(f'{up}\t{pdb_id}\tR-free above cutoff {free_r_cutoff}\n')
                    continue
            pdb_hit_bool = True
            break

        if not pdb_hit_bool:
            print(f"No (sufficient-quality) structure found for {up}")
            with open(failed_up_file, 'a') as fh: fh.write(f'{up}\tall\tno suitable pdb found\n')
            continue

        # Retrieve original pdb
        pdb_fn = pdb_downloader.retrieve_pdb_file(pdb_id, pdir=wd_raw, file_format='pdb')
        # Create Structure object
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            pdb_structure = pdb_parser.get_structure(pdb_id, pdb_fn)
        chain_id_str = pdb_structure.header['compound'][entity_id]['chain'].upper()
        if ',' in chain_id_str:
            chain_id_list = chain_id_str.split(', ')
        else:
            chain_id_list = [chain_id_str]

        # Check chains on validity, store one
        chain_hit_id = ''
        for chain_id in chain_id_list:
            chain = [ch for ch in pdb_structure.get_chains() if ch.get_id() == chain_id][0]
            [chain.detach_child(res.get_id()) for res in list(chain) if res.get_resname() == 'HOH']  #remove waters

            # Remove non-standard residues that were in first/last position
            res_list = list(chain.get_residues())
            caps = [res_list[0], res_list[-1]]
            while not all( [aa_dict_31.get(cap.get_resname(), False) for cap in caps]):
                [chain.detach_child(cap.get_id()) for cap in caps if not aa_dict_31.get(cap.get_resname(), False)]
                res_list = list(chain.get_residues())
                caps = [res_list[0], res_list[-1]]

            # Check for non-standard residues in other positions
            res_list = [res.get_resname() for res in chain.get_residues()]
            non_std_resnames = [res for res in res_list if not aa_dict_31.get(res, False)]
            if len(non_std_resnames): continue
            if len(res_list) < args.min_length: continue  # min sequence length check
            chain_hit_id = chain_id
            chain_hit = chain
            break
        if not len(chain_hit_id):
            print(f"No suitable chain found for {up}")
            with open(failed_up_file, 'a') as fh:
                fh.write(f'{up}\t{pdb_id}\tno suitable chain\n')
            continue

        # Save correct chain
        io = PDBIO()
        io.set_structure(chain_hit)
        io.save(f'{wd_clean}{up}_{pdb_id}_{entity_id}.pdb')


        # structure.save_tag_pdbs(wd_tagcoords)
        print(f'Stored coordinates for {up}')
    except Exception as e:
        print(f'Error caught! Logged to {errorlog_fn}: {e} ')
        with open(errorlog_fn, 'a') as efn:
            efn.write(f'{up}\t{e}\n')
